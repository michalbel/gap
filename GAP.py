import cPickle as cp
import copy
import json as js
import os
import pprint as pp
import sys
from functools import partial
from math import ceil, floor
from time import strftime

import guidata.dataset.dataitems as di
import guidata.dataset.datatypes as dt
import guiqwt.signals as guisigs
import iminuit as mn
import numpy as np
import scipy.optimize as so
import scipy.stats as sct
from PyQt4 import QtCore, QtGui
from guiqwt.builder import make as qmake
from guiqwt.plot import CurveDialog
from guiqwt.tools import LabelTool
from scipy.special import erf


# import warnings
# from warnings import catch_warnings

# warnings.simplefilter('ignore', mn.InitialParamWarning, append=False) #  suppressing iminuit initialization warnings

print "Be sure the data are in Angstroms\n"


# ------------ Functions definitions -----------------------------

# ------------ Dictionaries --------------------------------------

def getFromDict(dict, list):
    """
    function to retrieve the value from 'dict' on the position defined by address 'list'
    :param dict: a dictionary to retrieve the value from
    :param list: an address of in a nested structure of 'dict'
    :return: a value on 'list' position of 'dict'
    """
    return reduce(lambda dic, cf: dic[cf], list, dict)


def setInDict(dict, list, value):
    """
    function for in-place setting 'value' to the position 'list' in a nested structure of 'dict'
    :param dict: a targeted dictionary
    :param list: an address in a 'dict' nested structure
    :param value: a desired value to be assigned in 'dict' on 'list'-position
    :return: NULL
    """
    getFromDict(dict, list[:-1])[list[-1]] = value


# ------------ Structure/fitting functions -----------------------

def gauss(k, r, rmean, sig):
    """
    scaled gaussian function
    :param k: scaling constant (without normalization)
    :param r: (array) independent variable
    :param rmean: (float) the gaussian mean value
    :param sig: (float) the gaussian width
    :return: an array of the scaled gaussian values based on 'r'
    """
    return k * np.exp(-(r - rmean) ** 2.0 / (2.0 * sig ** 2.0))


def schulz(k, r, rmean, sig):
    """
    Schulz-Flory/Gamma-distribution function
    :param k: (float) scaling constant (after normalization)
    :param r: (array) independent variable
    :param rmean: (float) the mean distribution value
    :param sig: (float) the width of distribution
    :return: (array) the distribution values on 'r'
    """
    z = rmean * rmean / (sig * sig) - 1.0
    n = z + 1.0
    m = r / rmean * n

    return k * sct.gamma.pdf(m, n) * n / rmean


def lcheck(x):
    """
    simple function checking if 'x' is a list or a float. if it is a list it will return the first field of it. otherwise
    it returns only 'x'
    :param x: a float or a list
    :return: float
    """
    if type(x) == list:
        return x[0]
    elif type(x) == np.ndarray:
        return x[:, 0]
    else:
        return x


def expdist(k, r, rmean):
    """
    scaled exponential distribution function
    :param k: (float) scaling constant (without normalization)
    :param r: (array) independent input variable
    :param rmean: (float) the mean value of distribution
    :return: (array) distribution values on 'r'
    """
    return k * np.exp(-r / rmean)


def gauss_gr(s, z, sig):
    """
    Gaussian distribution function of a component group
    :param s: (array) independent input variable
    :param z: (float) the mean position of a component group on the bilayer normal
    :param sig: (float) the width of a component group
    :return: (array) component group distribution values on 'r'
    """
    return np.exp(-(s - z) * (s - z) / (2.0 * sig * sig)) / np.sqrt(2.0 * np.pi * sig * sig)


def core_gr(s, z, sig):
    """
    Plateau-function distribution representing a hydrocarbon core
    :param s: (array) independent input variable
    :param z: (float) position of the hydrocarbon core border (Dc)
    :param sig: (float) roughness of the hydrocarbon core
    :return: (array) distribution array
    """
    return 0.5 * (erf((s + z) / (np.sqrt(2.0) * sig)) - erf((s - z) / (np.sqrt(2.0) * sig)))


def res_arr(s, r, sig):
    """
    gaussian distribution for (optional) resolution implementation
    non-zero values only on 'mean +- 3*sig'
    :param s: (array) independent input values
    :param r: (float) mean gaussian value
    :param sig: (float) the width of a resolution window
    :return: (array) zero/non-zero-gaussian array
    """
    return gauss_gr(s, r, sig) if np.abs(s - r) < 3.0 * sig else 0.0


def ft_gaus_gr_real(s, z0, sig):
    """
    real part of the FT of a gaussian A-dependent group
    :param s: (array) q-values array
    :param z0: (float) the position of a gaussian on the bilayer normal
    :param sig: (float) the width of a gaussian
    :return: (array) array of the real values of FT-gaussian
    """
    return np.exp(-1.0j * s * z0 - 0.5 * s * s * sig * sig).real


def ft_gaus_gr_imag(s, z0, sig):
    """
    imaginary part of the FT of a gaussian A-dependent group
    :param s: (array) q-values array
    :param z0: (float) the position of a gaussian on the bilayer normal
    :param sig: (float) the width of a gaussian
    :return: (array) array of the imaginary values of FT-gaussian
    """
    return np.exp(-1.0j * s * z0 - 0.5 * s * s * sig * sig).imag


def ft_erf(s, r, sig):
    """
    FT of error-function defined through the gauss distribution
    :param s: (array) array of q-values in the reciprocal space
    :param r: (float) the mean position of the error function
    :param sig: (float) the width of the error-function/gaussian
    :return: (array) array of FT-error function on the q-array
    """
    return np.sin(s * r) * np.exp(-0.5 * s * s * sig * sig) / s


def ft_plateau_fun(s, r1, sig1, r2, sig2):
    """
    FT of a plateau-function between two error-functions placed at 'r1' and 'r2'
    with roughnesses 'sig1' and 'sig2 (r1 < r2)
    :param s: (array) q-values array at which the function is evaluated
    :param r1: (float) the position of the first plateau border
    :param sig1: (float) the roughness of the first plateau border
    :param r2: (float) the position of the second plateau border
    :param sig2: (float) the roughness of the second plateau border
    :return: (array) the array of FT-plateau-function values on the provided q-values array
    """
    return 0.5 * (ft_erf(s, r1, sig1) - ft_erf(s, r2, sig2))


def radm_micro(rtot, r0, c0, a0, a1, n0, n1,
               r0e=None, c0e=None, a0e=None, a1e=None, n0e=None, n1e=None):
    """
    returns molar admixture ratio to the lipid content in the second phase
    :param rtot: (float) total (the whole system) molar admixture ratio
    :param r0: (float) molar admixture ratio in the first phase
    :param c0: (float) outer mlv surface fraction formed by the first phase
    :param a0: (float) area per lipid in 1. phase
    :param a1: (float) area per lipid in 2. phase
    :param n0: (float) number of bilayers in the stack in 1. phase
    :param n1: (float) number of bilayers in the stack in 2. phase
    :param r0e: (float) error in r0
    :param c0e: (float) error in c0
    :param a0e: (float) error in a0
    :param a1e: (float) error in a1
    :param n0e: (float) error in n0
    :param n1e: (float) error in n1
    :return: (float, float) r1 (r1e)
    """

    if c0 == 1.0:
        if r0e is None:
            return 0.0
        else:
            return 0.0, 0.0

    else:
        cc = c0 / (1.0 - c0)
        aa = a1 / a0
        nn = n0 / n1
        rr = rtot - r0

        r1 = rr * cc * aa * nn + rtot

        if r0e is None:
            return r1

        else:
            r1e = np.sqrt(aa * aa * nn * nn * (cc * cc * r0e * r0e + rr * rr * c0e * c0e / (1.0 - c0e) ** 4.0) +
                          rr * rr * cc * cc * nn * nn * (a1e * a1e / a0 / a0 + aa * aa * a0e * a0e / a0 / a0) +
                          rr * rr * cc * cc * aa * aa * (n0e * n0e / n1 / n1 + nn * nn * n1e * n1e / n1 / n1))
            return r1, r1e


def radm_nano(rtot, r0, phiD, phiA, a0, a1, r0e=None, phiDe=None, phiAe=None, a0e=None, a1e=None):
    """
    !!! do not calculate the error of molar ratio
    returns molar admixture ratio to the lipid content in the second phase
    :param rtot: (float) total (the whole system) molar admixture ratio
    :param r0: (float) molar admixture ratio in the first phase
    :param phiD: (float) outer mlv surface fraction formed by the registered Ld-bilayer
    :param phiA: (float) outer mlv surface fraction formed by asymmetrical bilayers
    :param a0: (float) area per lipid in 1. phase
    :param a1: (float) area per lipid in 2. phase
    :param r0e: (float) error in r0
    :param phiDe: (float) error in phiD
    :param phiAe: (float) error in phiA
    :param a0e: (float) error in a0
    :param a1e: (float) error in a1
    :return: (float, float) r1 (r1e)
    """
    # TODO add error calculation/analysis

    res = 0.0
    res_err = 0.0

    if phiA == 0.0:
        if phiD == 1.0:
            res = float(rtot)
            if r0e is not None:
                res_err = 0.0
        else:
            cd = phiD * (1.0 - phiA) + 0.5 * phiA
            res = rtot + (rtot - r0) * cd / (1.0 - cd) * a1 / a0
    else:
        cd = phiD * (1.0 - phiA) + 0.5 * phiA
        res = rtot + (rtot - r0) * cd / (1.0 - cd) * a1 / a0

    if r0e is None:
        return res
    else:
        return res, res_err


def densProfLim(groups):
    """
    function giving the bilayer border on the bilayer normal defined as the position + 3 sigmas of the very outer group
    :param groups: (dict) (sub)dictionary containing groups properties
    :return: (float) the (total) border position
    """
    toplim = 0.0
    for grp in groups.keys():
        try:
            if groups[grp]['id'] in [0, 1]:
                if toplim < lcheck(groups[grp]['z']) + 3.0 * lcheck(groups[grp]['sig']):
                    toplim = lcheck(groups[grp]['z']) + 3.0 * lcheck(groups[grp]['sig'])
        except TypeError:
            pass

    return toplim


def densProfile(area, groups, div_mol=False, num_points=200, limit=50.0):
    """
    electron density profile function for a given phase
    :param z_arr: (array) array of z-values, on which f() should be evaluated
    :param area: (float) area per lipid molecule
    :param groups: (dict) dictionary containing component groups parameters
    :param div_mol: (float) admixture molecule division switch
    :return: (array) (absolute!) electron density profile of a single phase given by 'groups'
    """
    global dens_w

    ch_r = 0.0
    ch_sig = 0.0
    dens_carb = 0.0

    for grn in groups.keys():
        try:
            if groups[grn]['id'] == 3:
                dens_carb = lcheck(groups[grn]['dens'])
                ch_r = lcheck(groups[grn]['z'])
                ch_sig = lcheck(groups[grn]['sig'])
        except TypeError:
            pass

    z_arr = np.linspace(-limit, limit, num_points)
    a_dep = np.zeros(z_arr.shape)
    dens_arr = dens_w * np.ones(z_arr.shape)
    for nm in groups.keys():
        if type(groups[nm]) == dict and 'env' in groups[nm].keys():
            if groups[nm]['env'] == 'w':
                dens_env = float(dens_w)
            if groups[nm]['env'] == 'c':
                dens_env = float(dens_carb)

            if groups[nm]['id'] == 0:
                a_dep += gauss_gr(z_arr, lcheck(groups[nm]['z']), lcheck(groups[nm]['sig'])) \
                    * (lcheck(groups[nm]['ne']) - lcheck(groups[nm]['vol']) * dens_env)
                a_dep += gauss_gr(z_arr, -lcheck(groups[nm]['z']), lcheck(groups[nm]['sig'])) \
                    * (lcheck(groups[nm]['ne']) - lcheck(groups[nm]['vol']) * dens_env)

            if groups[nm]['id'] == 1:
                if not div_mol:
                    a_dep += gauss_gr(z_arr, lcheck(groups[nm]['z']), lcheck(groups[nm]['sig'])) \
                        * (lcheck(groups[nm]['ne']) - lcheck(groups[nm]['vol']) * dens_env) * lcheck(groups[nm]['relr'])
                    a_dep += gauss_gr(z_arr, -lcheck(groups[nm]['z']), lcheck(groups[nm]['sig'])) \
                        * (lcheck(groups[nm]['ne']) - lcheck(groups[nm]['vol']) * dens_env) * lcheck(groups[nm]['relr'])
                if div_mol:
                    a_dep += gauss_gr(z_arr, lcheck(groups[nm]['z']), lcheck(groups[nm]['sigh'])) \
                                 * (lcheck(groups[nm]['nehd']) - lcheck(groups[nm]['vol']) * \
                                 lcheck(groups[nm]['frhd']) * dens_env) * lcheck(groups[nm]['relr'])
                    a_dep += gauss_gr(z_arr, -lcheck(groups[nm]['z']), lcheck(groups[nm]['sigh'])) \
                        * (lcheck(groups[nm]['nehd']) - lcheck(groups[nm]['vol']) * lcheck(groups[nm]['frhd']) * dens_env) \
                        * lcheck(groups[nm]['relr'])
                    a_dep += gauss_gr(z_arr, lcheck(groups[nm]['z']) - lcheck(groups[nm]['zhdtl']),
                                    lcheck(groups[nm]['sigt'])) * (lcheck(groups[nm]['netl']) - lcheck(groups[nm]['vol'])
                                    * lcheck(groups[nm]['frtl']) * dens_env) * lcheck(groups[nm]['relr'])
                    a_dep += gauss_gr(z_arr, -lcheck(groups[nm]['z']) + lcheck(groups[nm]['zhdtl']),
                                    lcheck(groups[nm]['sigt'])) * (lcheck(groups[nm]['netl']) - lcheck(groups[nm]['vol'])
                                    * lcheck(groups[nm]['frtl']) * dens_env) * lcheck(groups[nm]['relr'])

    a_indep = core_gr(z_arr, ch_r, ch_sig) * (dens_carb - dens_w)

    return dens_arr + a_dep / area + a_indep


def phaseStructure(z_arr, area, groups, div_mol=False):
    """
    function returning the group structure of a phase in the form of a tuple containing curves for qwt-frame.
    :param z_array: (array) input z-values at which gaussians/plateau-functions should be evaluated
    :param area: (float) phase area per lipid
    :param groups: (dict) dictionary containing groups' parameters
    :param div_mol: (bool) dividing molecule switch
    :return: (tuple of arrays) group curves for a plotting frame
    """
    res = list()
    for nm in groups.keys():
        if type(groups[nm]) == dict and 'env' in groups[nm].keys():

            if groups[nm]['id'] == 0:
                res.append([(gauss_gr(z_arr, lcheck(groups[nm]['z']), lcheck(groups[nm]['sig'])) +
                           gauss_gr(z_arr, -lcheck(groups[nm]['z']), lcheck(groups[nm]['sig']))) *
                           lcheck(groups[nm]['vol']) / area, nm])

            if groups[nm]['id'] == 1:
                if not div_mol:
                    res.append([(gauss_gr(z_arr, lcheck(groups[nm]['z']), lcheck(groups[nm]['sig'])) +
                                 gauss_gr(z_arr, -lcheck(groups[nm]['z']), lcheck(groups[nm]['sig']))) *
                                lcheck(groups[nm]['vol']) * lcheck(groups[nm]['relr']) / area, nm])
                if div_mol:
                    res.append([(gauss_gr(z_arr, lcheck(groups[nm]['z']), lcheck(groups[nm]['sigh'])) +
                                 gauss_gr(z_arr, -lcheck(groups[nm]['z']), lcheck(groups[nm]['sigh']))) / area *
                                lcheck(groups[nm]['relr']) * lcheck(groups[nm]['vol']) *
                                lcheck(groups[nm]['frhd']), nm + "_H"])
                    res.append([(gauss_gr(z_arr, lcheck(groups[nm]['z']) - lcheck(groups[nm]['zhdtl']),
                               lcheck(groups[nm]['sigt'])) +
                               gauss_gr(z_arr, -lcheck(groups[nm]['z']) + lcheck(groups[nm]['zhdtl']),
                                        lcheck(groups[nm]['sigt']))) / area * lcheck(groups[nm]['relr']) *
                               lcheck(groups[nm]['vol']) * (1.0 - lcheck(groups[nm]['frhd'])), nm + "_T"])

            if groups[nm]['id'] == 3:
                res.append([core_gr(z_arr, lcheck(groups[nm]['z']), lcheck(groups[nm]['sig'])), nm])

    return res


def FormFactor(s, area, groups, div_mol=False):
    """
    formfactor parameterized by a parameter set in the form of a dictionary
    :param s: (array) inpdependent input q-array
    :param area: (float) surface area per lipid molecule
    :param groups: (dict) a dictionary containing parameter values of individual component groups
    :return: (array) the array of form factor values based on the q-array
    """
    global dens_w

    ch_r = 0.0
    ch_sig = 0.0
    for grn in groups.keys():
        try:
            if groups[grn]['id'] == 3:
                dens_carb = lcheck(groups[grn]['dens'])
                ch_r = lcheck(groups[grn]['z'])
                ch_sig = lcheck(groups[grn]['sig'])
        except TypeError:
            pass

    a_dep = np.zeros(s.shape)
    dens_env = 0.0
    # A-dependent part
    for nm in groups.keys():
        if type(groups[nm]) == dict and 'env' in groups[nm].keys():
            if groups[nm]['env'] == 'w':
                dens_env = float(dens_w)

            if groups[nm]['env'] == 'c':
                dens_env = float(dens_carb)

            if groups[nm]['id'] == 0:
                a_dep += 2.0 * ft_gaus_gr_real(s, lcheck(groups[nm]['z']), lcheck(groups[nm]['sig'])) \
                    * (groups[nm]['ne'] - lcheck(groups[nm]['vol']) * dens_env)

            if groups[nm]['id'] == 1:
                if not div_mol:
                    a_dep += 2.0 * ft_gaus_gr_real(s, lcheck(groups[nm]['z']), lcheck(groups[nm]['sig'])) \
                        * (lcheck(groups[nm]['ne']) - lcheck(groups[nm]['vol']) * dens_env) * lcheck(groups[nm]['relr'])

                if div_mol:
                    a_dep += 2.0 * ft_gaus_gr_real(s, lcheck(groups[nm]['z']), lcheck(groups[nm]['sigh'])) \
                                 * (lcheck(groups[nm]['nehd']) - lcheck(groups[nm]['vol']) * lcheck(groups[nm]['frhd']) * dens_env) \
                                 * lcheck(groups[nm]['relr'])
                    a_dep += 2.0 * ft_gaus_gr_real(s, lcheck(groups[nm]['z']) -
                                              lcheck(groups[nm]['zhdtl']), lcheck(groups[nm]['sigt'])) * \
                             (lcheck(groups[nm]['netl']) - lcheck(groups[nm]['vol']) *
                              lcheck(groups[nm]['frtl']) * dens_env) * lcheck(groups[nm]['relr'])

    a_indep = 2.0 * np.exp(-0.5 * s * s * ch_sig * ch_sig) * np.sin(ch_r * s) \
        / s * (dens_carb - dens_w)

    return a_dep / area + a_indep


def FormFacAsym(s, area1, grps1, area2, grps2, div_mol=False):
    """
    Provides the averaged form factor from asymmetrical parts of the bilayer
    :param s: (array) input q-values array
    :param area1: (float) area per lipid of the first phase (LD)
    :param grps1: (dict) dictionary containing the first phase groups parameters
    :param area2: (float) the aper per lipid in the second phase (LO
    :param grps2: (dict) dictionary containing the second phase groups parameters
    :param div_mol: (bool) switch to turn on/off admixture molecule parsing scheme
    :return: (array) averaged FF values from the asymmetrical parts of the bilayer based on q-values
    """
    global dens_w

    # ---> the roughness of the contact point of two plateau-functions in the middle of the bilayer
    # is set to 1.0, but later (by inner contrast variation technique) can be used as a free fitted parameter
    contact_sig = 1.0

    hc_z = []
    hc_sig = []
    dens_carb = []

    phs_grps = [grps1, grps2]
    AAs = [area1, area2]
    for grps in phs_grps:
        for grn in grps.keys():
            try:
                if grps[grn]['id'] == 3:
                    dens_carb.append(lcheck(grps[grn]['dens']))
                    hc_z.append(lcheck(grps[grn]['z']))
                    hc_sig.append(lcheck(grps[grn]['sig']))
            except TypeError:
                pass

    hc_bord = 0.5 * (hc_z[0] + hc_z[1])

    res = np.zeros(s.shape)

    dens_env = None
    for i in [0, 1]:
        a_dep = np.zeros(s.shape)
        hc_shift = hc_bord - hc_z[i]
        for nm in phs_grps[i].keys():
            if type(phs_grps[i][nm]) == dict and 'env' in phs_grps[i][nm].keys():
                if phs_grps[i][nm]['env'] == 'w':
                    dens_env = float(dens_w)

                if phs_grps[i][nm]['env'] == 'c':
                    dens_env = float(dens_carb[i])

                if phs_grps[i][nm]['id'] == 0:
                    a_dep += ft_gaus_gr_real(s, lcheck(phs_grps[i][nm]['z']) + hc_shift, lcheck(phs_grps[i][nm]['sig'])) \
                             * (phs_grps[i][nm]['ne'] - lcheck(phs_grps[i][nm]['vol']) * dens_env)

                if phs_grps[i][nm]['id'] == 1:
                    if not div_mol:
                        a_dep += ft_gaus_gr_real(s, lcheck(phs_grps[i][nm]['z']) + hc_shift, lcheck(phs_grps[i][nm]['sig'])) \
                                 * (lcheck(phs_grps[i][nm]['ne']) - lcheck(phs_grps[i][nm]['vol']) * dens_env) * lcheck(
                            phs_grps[i][nm]['relr'])

                    if div_mol:
                        a_dep += ft_gaus_gr_real(s, lcheck(phs_grps[i][nm]['z'])
                                            + hc_shift, lcheck(phs_grps[i][nm]['sigh'])) \
                                 * (lcheck(phs_grps[i][nm]['nehd']) - lcheck(phs_grps[i][nm]['vol'])
                                    * lcheck(phs_grps[i][nm]['frhd']) * dens_env) \
                                 * lcheck(phs_grps[i][nm]['relr'])

                        a_dep += ft_gaus_gr_real(s, lcheck(phs_grps[i][nm]['z']) -
                                            lcheck(phs_grps[i][nm]['zhdtl']) + hc_shift,
                                            lcheck(phs_grps[i][nm]['sigt'])) * (lcheck(phs_grps[i][nm]['netl'])
                                                                                - lcheck(phs_grps[i][nm]['vol'])
                                                                                * lcheck(phs_grps[i][nm]['frtl'])
                                                                                * dens_env) \
                            * lcheck(phs_grps[i][nm]['relr'])

        res += a_dep / AAs[i]

        if hc_shift <= 0.0:
            res += np.exp(-0.5 * s * s * hc_sig[i] * hc_sig[i]) * np.sin(hc_bord * s) \
                / s * (dens_carb[i] - dens_w)
            res += np.exp(-0.5 * s * s * contact_sig * contact_sig) * np.sin(hc_bord * s) \
                / s * (dens_carb[i] - dens_w)

        if hc_shift > 0.0:
            res += 0.5 * ft_plateau_fun(s, hc_bord - hc_z[i], contact_sig,
                                  hc_bord, hc_sig[i]) * (dens_carb[i] - dens_carb[i - 1])

    return res


def FormFac2Asym(s, area1, grps1, area2, grps2, div_mol=False):
    """
    the FF2 contribution from asymmetrical parts of the bilayer
    in this case it does not matter how the bilayer is oriented as only the squared magnitude matters
    :param s: (array) q-values
    :param area1: (float) area per lipid in the first kind of domains (LD)
    :param grps1: (dict) the groups parameters in the first kind of domains (LD)
    :param area2: (float) area per lipid in the second kind of domains (LO)
    :param grps2: (dict) the groups parameters in the second kind of domains (LO)
    :param div_mol: (bool) switch to turn on/off admixture molecule parsing scheme
    :return: (array) averaged squared modulus of FF from asymmetrical parts of the bilayer based on q-values
    """
    global dens_w

    # ---> the roughness of the contact point of two plateau-functions in the middle of the bilayer
    # is set to 1.0, but later (by inner contrast variation technique) can be used as a free fitted parameter
    contact_sig = 1.0

    res = np.array(s.shape)

    # sine part of asymFF
    sinFF = np.zeros(s.shape)

    hc_z = []
    hc_sig = []
    dens_carb = []

    phs_grps = [grps1, grps2]
    AAs = [area1, area2]
    for grps in phs_grps:
        for grn in grps.keys():
            try:
                if grps[grn]['id'] == 3:
                    dens_carb.append(lcheck(grps[grn]['dens']))
                    hc_z.append(lcheck(grps[grn]['z']))
                    hc_sig.append(lcheck(grps[grn]['sig']))
            except TypeError:
                pass

    hc_bord = 0.5 * (hc_z[0] + hc_z[1])

    # switch to control plateau-functions implementation
    carbswitch = 0
    for i in [0, 1]:
        a_dep = np.zeros(s.shape)
        hc_shift = hc_bord - hc_z[i]
        for nm in phs_grps[i].keys():
            try:
                if type(phs_grps[i][nm]) == dict and 'env' in phs_grps[i][nm].keys():
                    if phs_grps[i][nm]['env'] == 'w':
                        dens_env = float(dens_w)

                    if phs_grps[i][nm]['env'] == 'c':
                        dens_env = float(dens_carb[i])

                    if phs_grps[i][nm]['id'] == 0:
                        a_dep += ft_gaus_gr_imag(s, lcheck(phs_grps[i][nm]['z']) + hc_shift, lcheck(phs_grps[i][nm]['sig'])) \
                                 * (phs_grps[i][nm]['ne'] - lcheck(phs_grps[i][nm]['vol']) * dens_env)

                    if phs_grps[i][nm]['id'] == 1:
                        if not div_mol:
                            a_dep += ft_gaus_gr_imag(s, lcheck(phs_grps[i][nm]['z']) + hc_shift, lcheck(phs_grps[i][nm]['sig'])) \
                                     * (lcheck(phs_grps[i][nm]['ne']) - lcheck(phs_grps[i][nm]['vol']) * dens_env) * lcheck(
                                phs_grps[i][nm]['relr'])

                        if div_mol:
                            a_dep += ft_gaus_gr_imag(s, lcheck(phs_grps[i][nm]['z'])
                                                + hc_shift, lcheck(phs_grps[i][nm]['sigh'])) \
                                     * (lcheck(phs_grps[i][nm]['nehd']) - lcheck(phs_grps[i][nm]['vol'])
                                        * lcheck(phs_grps[i][nm]['frhd']) * dens_env) \
                                     * lcheck(phs_grps[i][nm]['relr'])

                            a_dep += ft_gaus_gr_imag(s, lcheck(phs_grps[i][nm]['z']) -
                                                lcheck(phs_grps[i][nm]['zhdtl']) + hc_shift,
                                                lcheck(phs_grps[i][nm]['sigt'])) * (lcheck(phs_grps[i][nm]['netl'])
                                                                                    - lcheck(phs_grps[i][nm]['vol'])
                                                                                    * lcheck(phs_grps[i][nm]['frtl'])
                                                                                    * dens_env) \
                                * lcheck(phs_grps[i][nm]['relr'])
            except TypeError:
                pass
        sinFF += a_dep / AAs[i]

        if hc_shift >= 0.0 and carbswitch == 0:
            # (carbswitch) both plateau functions from different domains are calculated at once. for the future,
            # there might be more elegant way to implement it. the problem is when DCs of both domains are the same
            # to not include them twice
            sinFF += ft_plateau_fun(s, -hc_bord, hc_sig[i], -hc_bord + hc_z[i], contact_sig) * (dens_carb[i] - dens_w)
            sinFF += ft_plateau_fun(s, -hc_bord + hc_z[i], contact_sig, hc_bord, hc_sig[i - 1]) * (dens_carb[i - 1] - dens_w)
            carbswitch = 1

    cosFF = FormFacAsym(s, area1, grps1, area2, grps2, div_mol)

    return sinFF * sinFF + cosFF * cosFF


def MonoStrucFac(s, strucPars):
    """
    Monodisperse structure factor
    :param s: (array) q-values
    :param strucPars: (dict) structure parameters
    :return: (array) structure factor values based on the q-values
    """
    global eulcon
    global fitSFDef

    nn = lcheck(strucPars['Nbil'])
    dd = lcheck(strucPars['Dsp'])
    et = lcheck(strucPars['Eta'])
    res = nn * np.ones(s.shape)

    if fitSFDef == 0:
        for i in range(1, int(nn)):
            res += 2.0 * (nn - float(i)) * np.cos(float(i) * s * dd) \
                * np.exp(-(0.5 * dd / np.pi) ** 2.0 * s * s
                         * et * eulcon) * (np.pi * float(i)) \
                ** (-(0.5 * dd / np.pi) ** 2.0 * s * s * et)
    if fitSFDef == 1:
        for i in range(1, int(nn)):
            res += 2.0 * (nn - float(i)) * np.cos(float(i) * s * dd) \
                * np.exp(-0.5 * float(i * i) * s * s * et * et)

    return res


def PolyStrucFac(s, strucPars):
    """
    polydisperse structure factor defined by MCT theory
    :param s: (array) q-values array
    :param strucPars: (dict) structure parameters
    :return: (array) structure factor based on q-values
    """
    global eulcon
    global fitDist
    global fitSFDef

    distarr = None
    Nmax = 0
    Nmean = lcheck(strucPars['Nbil'])
    sig = None
    if fitDist == 0:
        if Nmean < 5.0:
            sig = 0.5 * (Nmean - 1.0)
        if Nmean >= 5.0:
            sig = np.sqrt(Nmean)
        if Nmean > 7.0:
            awid = 3.0 * sig
        if Nmean <= 7.0:
            awid = 2.0 * sig
        Nmax = int(floor(Nmean + awid))
        Nmin = int(ceil(Nmean - awid))
        giter = np.fromiter((np.exp(-(float(i) - Nmean) ** 2.0 / (2.0 * sig * sig))
                             for i in xrange(Nmin, Nmax + 1)), np.float)
        garr = np.concatenate((np.zeros(Nmin), giter), axis=0)
        distarr = garr / garr.sum()
    if fitDist == 1:
        Nmax = int(floor(3.0 * float(Nmean) * np.log(10.0)))     # set to cut 1/1000 of the area under pdf graph
        exparr = np.exp(-(np.arange(1, Nmax + 2) - 1.0) / float(Nmean))
        distarr = exparr / (exparr.sum() - np.exp(-1.0 / Nmean))
    if fitDist == 2:
        sig = lcheck(strucPars['Nsig'])
        Nmin = max(0, int(ceil(Nmean - 5.0 * sig)))
        Nmax = int(floor(Nmean + 5.0 * sig))
        schiter = np.fromiter((schulz(1.0, float(i), Nmean, sig) for i in range(Nmin, Nmax + 1)), np.float)
        scharr = np.concatenate((np.zeros(Nmin), schiter), axis=0)
        distarr = scharr / scharr.sum()

    et = lcheck(strucPars['Eta'])
    thick = lcheck(strucPars['Dsp'])

    SF = np.zeros(s.shape)

    if fitSFDef == 0:
        t = - (0.5 * thick / np.pi) ** 2.0 * s * s * et
        for m in xrange(1, Nmax + 1):
            SF += 2.0 * (np.arange(1, Nmax - m + 1) *
                         distarr[m + 1:]).sum() * np.cos(float(m) * thick * s) * np.exp(t * np.log(m))
        SF *= np.exp(t * (eulcon + np.log(np.pi)))

    if fitSFDef == 1:
        for m in xrange(1, Nmax + 1):
            SF += 2.0 * (np.arange(1, Nmax - m + 1) *
                         distarr[m + 1:]).sum() * np.cos(float(m) * thick * s) \
                  * np.exp(-0.5 * float(m * m) * s * s * et * et)

    return Nmean + SF


def bground(s, p):
    """
    defines a background parameterized by a parameter list 'p'
    :param p: (list/array) list of parameters, len(p) > 0
    :return: (array) background
    """
    if len(p) == 1:
        res = float(p[0]) * np.ones(s.shape)
    else:
        res = p[0] + p[1] / np.power(s, p[2])

    return res


def fitfun(s, series_pars):
    """
    fitting function
    :param s: (array) input q-values
    :param series_pars: (dict) the whole set of imported model parameters
    :return: (array) total model intensity
    """
    global polydispersity
    global domainRegime
    global divadm

    if polydispersity == 0:
        SF = MonoStrucFac
    if polydispersity == 1:
        SF = PolyStrucFac

    FF = FormFactor

    fin_int = np.zeros(s.shape)
    ff_calc = list()
    sf_calc = list()

    cd1 = None

    if domainRegime == 0:
        for i in range(1, len(series_pars)):
            prs = series_pars[str(i)]['Prps']
            ndiff = lcheck(series_pars[str(i)]['Prps']['Ndiff'])
            cd = lcheck(series_pars[str(1)]['Prps']['Frct'])
            if i == 1:
                cd1 = float(cd)
            if i > 1:
                # just another control except pars_adjust
                cd = 1.0 - cd1
            aa = lcheck(series_pars[str(i)]['Prps']['Area'])
            grp = dict((key, value) for key, value in series_pars[str(i)].iteritems() if key != 'Prps')
            if divadm:
                ff_calc.append(FF(s, aa, grp, div_mol=True))
            else:
                ff_calc.append(FF(s, aa, grp))
            sf_calc.append(SF(s, prs))
            fin_int += cd * ((1.0 - ndiff) * sf_calc[-1] + ndiff) * np.abs(ff_calc[-1]) ** 2.0 / (s * s)
        # just for 'playing' purposes:
        if len(series_pars) > 2:
            if "CMixEff" in series_pars['0']['Prps'].keys() and "CrossScl" in series_pars['0']['Prps'].keys():
                ceff = lcheck(series_pars['0']['Prps']['CMixEff'])
                fin_int += lcheck(series_pars['0']['Prps']['CrossScl']) * \
                    (ceff * sf_calc[0] + (1.0 - ceff) * sf_calc[1] + lcheck(series_pars['0']['Prps']['absTerm'])) * \
                    np.abs(ff_calc[0] * ff_calc[1]) / (s * s)

    if domainRegime == 1:
        phiA = lcheck(series_pars["1"]['Prps']['AsymFr'])
        phiD = lcheck(series_pars["1"]['Prps']['Frct'])
        ndiff = lcheck(series_pars["1"]['Prps']['Ndiff'])
        fin_int = np.zeros(s.shape)

        # groups parameters divided into phases
        phasePrs = series_pars['1']['Prps']
        nbils = lcheck(phasePrs['Nbil'])
        grpPrs = []
        AAs = []
        for i in range(1, len(series_pars)):
            AAs.append(lcheck(series_pars[str(i)]['Prps']['Area']))
            grpPrs.append(dict((key, value) for key, value in series_pars[str(i)].iteritems() if key != 'Prps'))

        if divadm:
            ff_d = FF(s, AAs[0], grpPrs[0], divadm)
            ff2_d = ff_d * ff_d
            ff_o = FF(s, AAs[1], grpPrs[1], divadm)
            ff2_o = ff_o * ff_o
        else:
            ff_d = FF(s, AAs[0], grpPrs[0])
            ff2_d = ff_d * ff_d
            ff_o = FF(s, AAs[1], grpPrs[1])
            ff2_o = ff_o * ff_o

        ffAvg = phiD * ff_d + (1.0 - phiD) * ff_o
        ff2Avg = phiD * ff2_d + (1.0 - phiD) * ff2_o

        if phiA > 0.0:
            if divadm:
                ffAvg = (1.0 - phiA) * ffAvg + phiA * FormFacAsym(s, AAs[0], grpPrs[0], AAs[1], grpPrs[1], divadm)
                ff2Avg = (1.0 - phiA) * ff2Avg + phiA * FormFac2Asym(s, AAs[0], grpPrs[0], AAs[1], grpPrs[1], divadm)
            else:
                ffAvg = (1.0 - phiA) * ffAvg + phiA * FormFacAsym(s, AAs[0], grpPrs[0], AAs[1], grpPrs[1])
                ff2Avg = (1.0 - phiA) * ff2Avg + phiA * FormFac2Asym(s, AAs[0], grpPrs[0], AAs[1], grpPrs[1])

        ffAvg2 = ffAvg * ffAvg
        fin_int += ((1.0 - ndiff) * ((SF(s, phasePrs) - nbils) * ffAvg2 + nbils * ff2Avg) + ndiff * ffAvg2) / (s * s)

    return lcheck(series_pars['0']['Prps']['Scale']) * fin_int \
        + bground(s, lcheck(series_pars['0']['Prps']['Bckg']))


def fungen(var_name_list):
    """
    The purpose of this function is to provide the same function as chi2,
    but with the names of arguments, which were chosen by the user
    :param var_name_list: (list) fitted variable names
    :return: (function) wrapped chi2-function dependent only on fitted parameters
    """
    global trmnt

    fout = None

    n = len(var_name_list)
    if n == 0:
        print "No parameters\n"
        trmnt = True
        pass
    strf = "def fout("
    strvar = var_name_list[0]
    for i in range(1, n):
        strvar += "," + var_name_list[i]
    strf += strvar + "):\n   return chi2([" + strvar + "])"

    exec strf

    return fout


def pars_adjust(parsall, errs=False, init=False):
    """
    function to adjust parameters values
    also serves as the place where new, even non-structural parameters can be introduced
    therefore this time the whole parameter set (even ones belonging to different data sets are included)
    :param pars: (dict) parameter set
    :param errs: (bool) switch to adjust parameters errors or not
    :param init: (bool) switch to indicate if an adjust is initial or not. initial == connheadson and fixheadson
    :return: (dict) adjusted parameters values
    """
    global connheadson
    global fixheadson
    global conn_adm_on
    global connkeys
    global parsCurr
    global fitDist, polydispersity
    global domainRegime

    parsout = []
    Vhc = 0.0
    for sern in range(len(parsall)):
        parsin = copy.deepcopy(parsall[sern])

        if len(parsin) == 3:
            if domainRegime == 1:
                cd = copy.deepcopy(parsin['1']['Prps']['Frct'])
                parsin['2']['Prps']['Frct'][0] = 1.0 - lcheck(cd)
                parsin['2']['Prps']['Dsp'] = copy.deepcopy(parsin['1']['Prps']['Dsp'])
                parsin['2']['Prps']['Nbil'] = copy.deepcopy(parsin['1']['Prps']['Nbil'])
                parsin['2']['Prps']['Nsig'] = copy.deepcopy(parsin['1']['Prps']['Nsig'])
                parsin['2']['Prps']['Eta'] = copy.deepcopy(parsin['1']['Prps']['Eta'])
                parsin['2']['Prps']['Ndiff'] = copy.deepcopy(parsin['1']['Prps']['Ndiff'])
            if type(parsin['2']['Prps']['Frct']) is list:
                parsin['2']['Prps']['Frct'][0] = 1.0 - parsin['1']['Prps']['Frct'][0]
                if errs:
                    parsin['2']['Prps']['Frct'][1] = float(parsin['1']['Prps']['Frct'][0])

            for nm in parsin['1'].keys():
                if type(parsin['1'][nm]) is dict:
                    if parsin['1'][nm]['id'] == 1 and type(parsin['1'][nm]['relr']) is list:
                        try:
                            if domainRegime == 0:
                                if not errs:
                                    parsin['2'][nm]['relr'][0] = \
                                        radm_micro(lcheck(parsin['1'][nm]['globr']), lcheck(parsin['1'][nm]['relr']),
                                              lcheck(parsin['1']['Prps']['Frct']), lcheck(parsin['1']['Prps']['Area']),
                                              lcheck(parsin['2']['Prps']['Area']), lcheck(parsin['1']['Prps']['Nbil']),
                                              lcheck(parsin['2']['Prps']['Nbil']))
                                if errs:
                                    parsin['2'][nm]['relr'][0], parsin['2'][nm]['relr'][1] = \
                                        radm_micro(lcheck(parsin['1'][nm]['globr']), lcheck(parsin['1'][nm]['relr']),
                                              lcheck(parsin['1']['Prps']['Frct']), lcheck(parsin['1']['Prps']['Area']),
                                              lcheck(parsin['2']['Prps']['Area']), lcheck(parsin['1']['Prps']['Nbil']),
                                              lcheck(parsin['2']['Prps']['Nbil']),
                                              parsin['1'][nm]['relr'][1],
                                              parsin['1']['Prps']['Frct'][1], parsin['1']['Prps']['Area'][1],
                                              parsin['2']['Prps']['Area'][1], parsin['1']['Prps']['Nbil'][1],
                                              parsin['2']['Prps']['Nbil'][1])
                            if domainRegime == 1:
                                if not errs:
                                    parsin['2'][nm]['relr'][0] = \
                                        radm_nano(lcheck(parsin['1'][nm]['globr']), lcheck(parsin['1'][nm]['relr']),
                                                  lcheck(parsin['1']['Prps']['Frct']),
                                                  lcheck(parsin['1']['Prps']['AsymFr']),
                                                  lcheck(parsin['1']['Prps']['Area']),
                                                  lcheck(parsin['2']['Prps']['Area']))
                                if errs:
                                    parsin['2'][nm]['relr'][0], parsin['2'][nm]['relr'][1] = \
                                        radm_nano(lcheck(parsin['1'][nm]['globr']), lcheck(parsin['1'][nm]['relr']),
                                                  lcheck(parsin['1']['Prps']['Frct']),
                                                  lcheck(parsin['1']['Prps']['AsymFr']),
                                                  lcheck(parsin['1']['Prps']['Area']),
                                                  lcheck(parsin['2']['Prps']['Area']),
                                                  parsin['1'][nm]['relr'][1], parsin['1']['Prps']['Frct'][1],
                                                  parsin['1']['Prps']['AsymFr'][1], parsin['1']['Prps']['Area'][1],
                                                  parsin['2']['Prps']['Area'][1])
                        except KeyError:
                            print "Probably a wrongly named or a missing group."
                            print "Check again groups in both phases."

        for phs in [str(i) for i in range(1, len(parsin))]:
            if fitDist == 1:
                if type(parsin[phs]['Prps']['Nsig']) is list:
                    parsin[phs]['Prps']['Nsig'][0] = float(parsin[phs]['Prps']['Nbil'][0])
                    parsin[phs]['Prps']['Nsig'][1] = float(parsin[phs]['Prps']['Nbil'][1])
                else:
                    parsin[phs]['Prps']['Nsig'] = float(parsin[phs]['Prps']['Nbil'][0])
            if fitDist == 0:
                if polydispersity == 0:
                    if type(parsin[phs]['Prps']['Nsig']) is list:
                        parsin[phs]['Prps']['Nsig'][0] = 1.0
                        parsin[phs]['Prps']['Nsig'][1] = 0.0
                    else:
                        parsin[phs]['Prps']['Nsig'] = 0.0
                if polydispersity == 1:
                    if type(parsin[phs]['Prps']['Nsig']) is list:
                        parsin[phs]['Prps']['Nsig'][0] = np.sqrt(parsin[phs]['Prps']['Nbil'][0])
                        parsin[phs]['Prps']['Nsig'][1] = np.abs(0.5 * float(parsin[phs]['Prps']['Nbil'][1])
                                                                / np.sqrt(parsin[phs]['Prps']['Nbil'][0]))
                    else:
                        parsin[phs]['Prps']['Nsig'] = np.sqrt(parsin[phs]['Prps']['Nbil'][0])

            Vhc = 0.0
            for nm in parsin[phs].keys():
                try:
                    if parsin[phs][nm]['env'] == 'c':
                        if parsin[phs][nm]['id'] == 0:
                            Vhc += lcheck(parsin[phs][nm]['vol'])
                        if parsin[phs][nm]['id'] == 1:
                            Vhc += lcheck(parsin[phs][nm]['vol']) * lcheck(parsin[phs][nm]['relr'])
                except KeyError:
                    pass
                except TypeError:
                    pass
            for nm in parsin[phs].keys():
                if type(parsin[phs][nm]) is dict:
                    if parsin[phs][nm]['id'] == 3:
                        Vhc += lcheck(parsin[phs][nm]['vol'])
                        parsin[phs][nm]['z'] = Vhc / lcheck(parsin[phs]['Prps']['Area'])
            # calculating Luzzati thickness:

            Vcomp = float(Vhc)
            for nm in parsin[phs].keys():
                if type(parsin[phs][nm]) is dict:
                    try:
                        if parsin[phs][nm]['env'] == 'w':
                            if parsin[phs][nm]['id'] == 0:
                                Vcomp += lcheck(parsin[phs][nm]['vol'])
                            if parsin[phs][nm]['id'] == 1:
                                Vcomp += lcheck(parsin[phs][nm]['vol']) * lcheck(parsin[phs][nm]['relr'])
                    except KeyError:
                        pass
                    except ValueError:
                        pass
            parsin[phs]['Prps']['DB'] = 2.0 * Vcomp / lcheck(parsin[phs]['Prps']['Area'])
            if errs:
                db_err2 = 0.0
                dc_err2 = 0.0
                for nm in parsin[phs].keys():
                    if type(parsin[phs][nm]) is dict:
                        try:
                            if parsin[phs][nm]['id'] in [0, 1, 3]:
                                if parsin[phs][nm]['id'] == 1:
                                    if type(parsin[phs][nm]['vol']) is list:
                                        if type(parsin[phs][nm]['vol']) == list:
                                            v1err = parsin[phs][nm]['vol'][1] * lcheck(parsin[phs][nm]['relr']) / \
                                                    lcheck(parsin[phs]['Prps']['Area'])
                                        db_err2 += v1err * v1err
                                        if type(parsin[phs][nm]['relr']) == list:
                                            v1err += lcheck(parsin[phs][nm]['vol']) * parsin[phs][nm]['relr'][1] \
                                                / lcheck(parsin[phs]['Prps']['Area'])
                                        db_err2 += v1err * v1err
                                        if parsin[phs][nm]['env'] == 'c':
                                            dc_err2 += v1err
                                    else:
                                        pass
                                else:
                                    if type(parsin[phs][nm]['vol']) is list:
                                        v2err = parsin[phs][nm]['vol'][1] / lcheck(parsin[phs]['Prps']['Area'])
                                        db_err2 += v2err * v2err
                                        if parsin[phs][nm]['env'] == 'c':
                                            dc_err2 += v2err
                        except KeyError:
                            pass
                        except ValueError:
                            pass
                db_err2 += (Vcomp * parsin[phs]['Prps']['Area'][1] / lcheck(parsin[phs]['Prps']['Area']) ** 2.0) ** 2.0
                dc_err2 += (Vhc * parsin[phs]['Prps']['Area'][1] / lcheck(parsin[phs]['Prps']['Area']) ** 2.0) ** 2.0
                parsin[phs]['Prps']['DB_err'] = 2.0 * np.sqrt(db_err2)
                parsin[phs]['Chains']['z_err'] = np.sqrt(dc_err2)

        # shifting polar head groups positions at the initialization
        if init:
            for phs in [str(i) for i in range(1, len(parsin))]:
                di = 0.0
                for nm in parsin[phs].keys():
                    try:
                        if parsin[phs][nm]['id'] == 3:
                            di = parsin[phs][nm]['z'] - parsCurr[sern][phs][nm]['z']
                    except TypeError:
                        pass
                for nm in parsin[phs].keys():
                    try:
                        if parsin[phs][nm]['id'] == 0 and parsin[phs][nm]['env'] == 'w':
                            parsin[phs][nm]['z'][0] += di
                    except TypeError:
                        pass

        if connheadson or fixheadson:
            for phs in [str(i) for i in range(1, len(parsin))]:
                dc = 0.0
                if connheadson:
                    for nm in parsin[phs].keys():
                        if type(parsin[phs][nm]) is dict and parsin[phs][nm]['id'] == 3:
                            dc = lcheck(parsin[phs][nm]['z']) - lcheck(parsCurr[sern][phs][nm]['z'])
                    parsin[phs][connkeys[sern][int(phs)]]['z'][0] += dc
                    if errs and dc != 0.0:
                        parsin[phs][connkeys[sern][int(phs)]]['z'][1] = float(parsin[phs]['Chains']['z_err'])
                if fixheadson:
                    polgrs = []
                    for nm in parsin[phs].keys():
                        if (type(parsin[phs][nm]) is dict and parsin[phs][nm]['id'] == 0) and parsin[phs][nm]['env'] == 'w':
                            if nm == connkeys[sern][int(phs)]:
                                df = parsin[phs][nm]['z'][0] - lcheck(parsCurr[sern][phs][nm]['z'])
                                dferr = parsin[phs][nm]['z'][1]
                            else:
                                polgrs.append(nm)
                    for grp in polgrs:
                        parsin[phs][grp]['z'][0] += df
                        if errs:
                            parsin[phs][grp]['z'][1] = float(dferr)
                            # parsin[phs][grp]['z'][1] = float(parsin[phs][connkeys[sern][int(phs)]][1])
        if conn_adm_on:
            for phs in [str(i) for i in range(1, len(parsin))]:
                for nm in parsin[phs].keys():
                    if type(parsin[phs][nm]) is dict and parsin[phs][nm]['id'] == 3:
                        hc_shift = lcheck(parsin[phs][nm]['z']) - lcheck(parsCurr[sern][phs][nm]['z'])
                for nm in parsin[phs].keys():
                    if type(parsin[phs][nm]) is dict and parsin[phs][nm]['id'] == 1:
                        parsin[phs][nm]['z'][0] += hc_shift
                        if errs:
                            parsin[phs][nm]['z'][1] = float(parsin[phs]['Chains']['z_err'])

        parsout.append(parsin)

    return parsout


def fitp2modp(fitPars, modPars, fitParsErr=None):
    """
    a function to input new values of the fitted parameters into the whole model parameter dictionary,
    with its deepcopy (ofc) and further adjustments
    :param fitPars: (list) fitted parameters values
    :param modPars: (dict) (deepcopied!) model parameters
    :return: (dict) (deepcopied) model parameters with inserted and adjusted values
    """
    global trmnt
    global bupParsCurr
    global bupFitPSwitch
    global fitParAddrs

    parsOut = copy.deepcopy(modPars)
    nonzrinds = bupFitPSwitch.nonzero()[0]
    if len(nonzrinds) != len(fitPars):
        trmnt = True
        return None
    for i in range(len(nonzrinds)):
        setInDict(parsOut, fitParAddrs[nonzrinds[i]] + [0], fitPars[i])
    if fitParsErr is not None:
        for i in range(len(nonzrinds)):
            setInDict(parsOut, fitParAddrs[nonzrinds[i]] + [1], fitParsErr[i])
        parsOut = pars_adjust(parsOut, errs=True)
    if fitParsErr is None:
        parsOut = pars_adjust(parsOut)

    return parsOut


def chi2(fitpars):
    """
    fuction defining non-reduced weighted chi2-function. the function used for fitting is defined in fitfun()
    backed-up variables are initiated elsewhere, here they are just used.
    :param fitpars: (list) fitting parameters (defined by fitParSwitch and fitParAddrs)
    :return:(bool) chi2-value
    """
    global trmnt
    global expdata
    global bupqlims
    global bupParsCurr
    global bupFitParLims

    result = 0.0
    if not trmnt:
        if bupqlims is None or bupParsCurr is None:
            bupqlims = np.array(qlims)
            bupParsCurr = copy.deepcopy(parsCurr)

        altPars = fitp2modp(fitpars, bupParsCurr)
        if not trmnt:
            for dset in range(len(expdata)):
                inq = np.where((expdata[dset, :, 0] > bupqlims[0])
                               & (expdata[dset, :, 0] < bupqlims[1]))[0]
                minin = inq.min()
                maxin = inq.max()
                Ires = fitfun(expdata[dset, minin: maxin + 1, 0], altPars[dset])
                resi = (Ires - expdata[dset, minin: maxin + 1, 1]) / expdata[dset, minin: maxin + 1, 2]
                result += (resi * resi).sum()

    if trmnt:
        for i in range(len(fitpars)):
            result += fitpars[i] * fitpars[i]

    return result


class mnfit(object):
    """
    class of MINUIT fitting procedure
    structure of setup parameters is obtained by the first minuit initialization, then they are modified
    and included into minuit by the second initialization
    setup of fitting parameters is done as soon as possible to restrict unexpected interaction from a user
    !! limits of a parameter has a form of a tuple
    """

    def __init__(self, numiter=10000, tol=0.1):
        global mainwin
        super(mnfit, self).__init__()
        self.par_names = list(mainwin.resFit[0])
        self.par_vals = list(mainwin.resFit[1])
        self.par_errs = list(mainwin.resFit[2])
        self.par_lims = list(mainwin.resFit[3])
        self.numiter = numiter
        self.tol = tol
        self.mm_in = None
        self.mm = None
        self.myargs = None
        self.minf = None

    def Fit(self):
        global trmnt
        global fitalg

        fitalg = 0

        self.minf = fungen(self.par_names)
        if not trmnt:
            # TODO add errodef to Minuit arguments according to iminuit documentation
            self.mm_in = mn.Minuit(self.minf, pedantic=False, print_level=0)
            self.myargs = self.mm_in.fitarg
            for i in range(len(self.par_names)):
                self.myargs[self.par_names[i]] = self.par_vals[i]
                if self.par_errs[i] != 0.0:
                    self.myargs['error_' + self.par_names[i]] = self.par_errs[i]
                if self.par_lims[i] is not None:
                    self.myargs['limit_' + self.par_names[i]] = tuple(self.par_lims[i])
            self.mm = mn.Minuit(self.minf, pedantic=False, print_level=1,
                                **self.myargs)
            self.mm.set_strategy(2)
            self.mm.tol = self.tol
            self.mm.migrad(ncall=self.numiter, resume=False)

    def GetPars(self):
        global mainwin

        mainwin.resFit[1] = [self.mm.values[x] for x in self.par_names]
        mainwin.resFit[2] = [self.mm.errors[x] for x in self.par_names]

    def GetStats(self):

        output = [["func_min_val", self.mm.fval],
                  ["vert_est_dist_min", self.mm.edm],
                  ["up_value", self.mm.up],
                  ["numb_par_fit", self.mm.ncalls]]
        return output


class defit(object):
    """
    class of fitting procedure. parameters: (details in scipy.optimize.differential_optimization \
    page). all the parameters have default values:
    strategy: the way how compared candidates are constructed
    maxiter: the maximum number the entire population is evolved. function is evaluated
            maxiter * popsize * len(x) times
    popsize: MULTIPLIER of ppulation size. the size of population is maxiter * popsize * len(x)
    tol: scales the condition of convergence
    mutation: the rate of mutation of parameters. if it is a interval its value randomly changes
        in gaiven interval per each eavluation.
    recombination: [0, 1] higher the number, more mutants moves into a next genaration
    seed: seed number for random number generators
    disp: display messages or not
    polish: states if the end state is polished after a successful minimalization
    init: the distribution of starting population. 'latinhypercube' tries to cover the most of
        the parameter space, 'random' produces purely random distribution, which may lead to
        the params space clustering
    """
    def __init__(self, strategy='best1bin', maxiter=10000, popsize=15, tol=0.001,
                 mutation=(0.5, 1.0), recombination=0.7, seed=None, disp=True, polish=True,
                 init='latinhypercube'):
        global trmnt
        global mainwin
        super(defit, self).__init__()
        self.par_names = list(mainwin.resFit[0])
        self.par_values = list(mainwin.resFit[1])
        self.par_errs = list(mainwin.resFit[2])
        self.par_lims = list(mainwin.resFit[3])
        self.strategy = strategy
        self.maxiter = maxiter
        self.popsize = popsize
        self.tol = tol
        self.mutation = mutation
        self.recombination = recombination
        self.seed = seed
        self.disp = disp
        self.polish = polish
        self.init = init
        self.res = None

    def Fit(self):
        global trmnt
        global fitalg

        fitalg = 1

        if not trmnt:
            print self.par_names, self.par_values, self.par_errs, self.par_lims
            # function chi2 is already in a very specific format required diff_evo algorithm
            self.res = so.differential_evolution(chi2, self.par_lims, strategy=self.strategy,
                                                 maxiter=self.maxiter, popsize=self.popsize,
                                                 tol=self.tol, mutation=self.mutation,
                                                 recombination=self.recombination, seed=self.seed,
                                                 disp=self.disp,
                                                 polish=self.polish, init=self.init)
        fitalg = 0
        return self.res

    def GetPars(self):
        global mainwin

        vals = list(self.res.x)
        mainwin.resFit[1] = list(vals)

        print '\n**************************************************'
        print '          Differential evolution fit:'
        print '                                     '
        for i in range(len(vals)):
            print '  ', self.par_names[i], '=', vals[i]
        print '\n**************************************************\n'

    def GetStats(self):
        output = [["number_of_evals", self.res.nfev],
                  ["success", self.res.success],
                  ["fun_value", self.res.fun],
                  ["number_of_iterations", self.res.nit],
                  ["fin_message", self.res.message]]
        return output


class fitThread(QtCore.QThread):
    """docstring for fitThread"""
    # the whole system of parameters is cloned here, as they should not be 'touched'
    # by a user during the fitting process. they will be stored as global parameters
    # for creation of a new set after a successful fitting
    # the choice of fitting procedures is included in the form of variables, as then they are bound to a given thread
    # beside the model pars also scale/back pars are backed up
    def __init__(self, parent=None, devfit=False, minfit=False):
        QtCore.QThread.__init__(self, parent)
        self.devfit = devfit  # swicth
        self.minfit = minfit  # switch
        self.minmin = None
        self.devmin = None
        self.res = None

    def run(self):
        global trmnt
        global fitalg

        if (not self.devfit) and (not self.minfit):
            self.emit(QtCore.SIGNAL("no_meth()"))

        if not trmnt:
            if self.devfit:
                try:
                    self.devmin = defit()
                    self.res = self.devmin.Fit()
                except:
                    pass
                if (self.res is None) or (not self.res['success']):
                    print "Fitting not successful.\n Values omitted. Process terminated.\n"
                    trmnt = True
                if trmnt:
                    os.system('cls' if os.name == 'nt' else 'clear')
                    self.exit(2)
                if not trmnt:
                    self.devmin.GetPars()

        if not trmnt:
            if self.minfit:
                try:
                    self.minmin = mnfit() # defined this way, not as = mnfit
                    self.minmin.Fit()
                except:
                    pass
                if trmnt:
                    os.system('cls' if os.name == 'nt' else 'clear')
                    self.exit(2)
                if not trmnt:
                    self.minmin.GetPars()

        if not trmnt:
            self.emit(QtCore.SIGNAL("fit_ended()"))


class LoadData(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(LoadData, self).__init__(parent)

        self.setWindowTitle("Loading data files")

        self.centralWidget = QtGui.QWidget(self)
        self.setCentralWidget(self.centralWidget)
        self.mainLayout = QtGui.QVBoxLayout(self.centralWidget)
        self.nDataLayout = QtGui.QGridLayout()
        self.mainLayout.addLayout(self.nDataLayout)
        self.boxLayout = QtGui.QHBoxLayout()
        self.mainLayout.addLayout(self.boxLayout)
        self.leftSdLayout = QtGui.QVBoxLayout()
        self.rightSdLayout = QtGui.QVBoxLayout()
        self.boxLayout.addLayout(self.leftSdLayout)
        self.boxLayout.addLayout(self.rightSdLayout)

        self.lbData = QtGui.QLabel("Nr. of datasets:  ", self.centralWidget)
        self.nDataLayout.addWidget(self.lbData, 0, 0)
        self.nrData = 1
        self.nrCombo = QtGui.QComboBox(self.centralWidget)
        self.nrCombo.setFixedWidth(35)
        self.nrCombo.insertItem(1, QtCore.QString("1"))
        self.nrCombo.insertItem(2, QtCore.QString("2"))
        self.nrCombo.insertItem(3, QtCore.QString("3"))
        self.nrCombo.insertItem(4, QtCore.QString("4"))
        self.nrCombo.insertItem(5, QtCore.QString("5"))
        self.connect(self.nrCombo, QtCore.SIGNAL("activated(int)"), partial(self.chng_num_data, self.nrCombo))
        self.nDataLayout.addWidget(self.nrCombo, 0, 1)
        self.nDataLayout.setColumnStretch(10, 2)

        self.dtLines = []
        self.dataBox = QtGui.QGroupBox("Data sets", self.centralWidget)
        self.dtLayout = QtGui.QVBoxLayout(self.dataBox)
        for i in range(5):
            self.lineLayout = QtGui.QHBoxLayout()
            self.lineLayout.setAlignment(QtCore.Qt.AlignLeft)
            self.lbLine = QtGui.QLabel("Set " + str(i + 1), self.dataBox)
            self.lineLayout.addWidget(self.lbLine)
            self.line = QtGui.QLineEdit(self.dataBox)
            if i > 0:
                self.line.setDisabled(True)
            self.dtLines.append(self.line)
            self.lineLayout.addWidget(self.line)
            self.oButt = QtGui.QPushButton(self.dataBox)
            self.oButt.setIcon(QtGui.QIcon(".\\images\\filopen.png"))
            self.connect(self.oButt, QtCore.SIGNAL("clicked()"), partial(self.openFile,
                                                                         "*.dat;;*.*", self.line))
            self.lineLayout.addWidget(self.oButt)
            self.dtLayout.addLayout(self.lineLayout)
        self.leftSdLayout.addWidget(self.dataBox)

        self.prLines = []
        self.parsBox = QtGui.QGroupBox("Parameter files", self.centralWidget)
        self.prLayout = QtGui.QVBoxLayout(self.parsBox)
        for i in range(5):
            self.lineLayout = QtGui.QHBoxLayout()
            self.lineLayout.setAlignment(QtCore.Qt.AlignLeft)
            self.lbLine = QtGui.QLabel("Pars #" + str(i + 1), self.parsBox)
            self.lineLayout.addWidget(self.lbLine)
            self.line = QtGui.QLineEdit(self.parsBox)
            if i > 0:
                self.line.setDisabled(True)
            self.prLines.append(self.line)
            self.lineLayout.addWidget(self.line)
            self.oButt = QtGui.QPushButton(self.parsBox)
            self.oButt.setIcon(QtGui.QIcon(".\\images\\filopen.png"))
            self.connect(self.oButt, QtCore.SIGNAL("clicked()"), partial(self.openFile, "*.json;;*.*", self.line))
            self.lineLayout.addWidget(self.oButt)
            self.prLayout.addLayout(self.lineLayout)
        self.rightSdLayout.addWidget(self.parsBox)

        self.buttLayout = QtGui.QHBoxLayout()
        self.mainLayout.addLayout(self.buttLayout)
        self.okButt = QtGui.QPushButton("OK", self.centralWidget)
        self.okButt.setFixedWidth(75)
        self.connect(self.okButt, QtCore.SIGNAL("clicked()"), self.pushOK)
        self.buttLayout.addWidget(self.okButt)
        self.cancelButt = QtGui.QPushButton("Cancel", self.centralWidget)
        self.cancelButt.setFixedWidth(75)
        self.connect(self.cancelButt, QtCore.SIGNAL("clicked()"), self.pushCancel)
        self.buttLayout.addWidget(self.cancelButt)
        self.buttLayout.setAlignment(QtCore.Qt.AlignRight)

    def chng_num_data(self, combo):
        """
        Changing the number of datasets
        """
        self.nrData = int(combo.currentIndex()) + 1

        for i in range(len(self.dtLines)):
            if i < self.nrData:
                self.dtLines[i].setEnabled(True)
                self.prLines[i].setEnabled(True)
            else:
                self.dtLines[i].setDisabled(True)
                self.prLines[i].setDisabled(True)

    def openFile(self, filter, line):
        """
        action after clicking line
        """
        try:
            fileName = QtGui.QFileDialog.getOpenFileName(self, "Select file...", filter=filter)
            line.setText(fileName)
        except IOError:
            pass

    def pushOK(self):
        global mainwin
        global fitWins
        global parsCurr
        global bupParsCurr
        global expdata
        global connkeys
        global qlims
        global bupqlims
        global fitParAddrs
        global fitParSwitch
        global bupFitPSwitch
        global fitParLims
        global bupFitParLims
        global parsHist
        global expdata

        parsCurr = []
        expdata = []
        for i in range(self.nrData):
            fname = self.prLines[i].text()
            try:
                pfile = open(fname)
                parin = js.load(pfile)
                parsCurr.append(parin)
            except ValueError:
                print "Wrong format of a parameter file:", fname
                sys.exit("Application terminated")
            dfile = str(self.dtLines[i].text())
            try:
                din = np.array(np.loadtxt(dfile))
                din = din[din[:, 0].argsort()]
                expdata.append(din)
            except ValueError:
                print "Wrong format of a data file:", dfile
                sys.exit("Application terminated")
        expdata = np.array(expdata)

        # changing background parameter list to an array
        for i in range(len(parsCurr)):
            parsCurr[i]['0']['Prps']['Bckg'] = np.array(parsCurr[i]['0']['Prps']['Bckg'])

        for sern in range(len(parsCurr)):
            serkeys = [[]]
            for i in range(1, len(parsCurr[sern])):
                minkey = None
                minval = None
                first = True
                if str(i) in parsCurr[sern].keys():
                    for nm in parsCurr[sern][str(i)].keys():
                        try:
                            if 'id' in parsCurr[sern][str(i)][nm].keys():
                                if parsCurr[sern][str(i)][nm]['id'] == 0 and parsCurr[sern][str(i)][nm]['env'] == 'w':
                                    if first:
                                        minkey = str(nm)
                                        minval = lcheck(parsCurr[sern][str(i)][nm]['z'])
                                        first = False
                                    else:
                                        if lcheck(parsCurr[sern][str(i)][nm]['z']) < minval:
                                            minval = lcheck(parsCurr[sern][str(i)][nm]['z'])
                                            minkey = str(nm)
                        except AttributeError:
                            pass
                serkeys.append(minkey)
            connkeys.append(serkeys)

        # format [[series, phase, group, parameter]]
        fitParAddrs = []
        for ser in range(len(parsCurr)):
            for phs in range(len(parsCurr[ser])):
                for nm in parsCurr[ser][str(phs)].keys():
                    if type(parsCurr[ser][str(phs)][nm]) is dict:
                        for pr in parsCurr[ser][str(phs)][nm].keys():
                            if type(parsCurr[ser][str(phs)][nm][pr]) is list:
                                fitParAddrs.append([ser, str(phs), nm, pr])
                            if type(parsCurr[ser][str(phs)][nm][pr]) is np.ndarray:
                                for i in range(len(parsCurr[ser][str(phs)][nm][pr])):
                                    fitParAddrs.append([ser, str(phs), nm, pr, i])

        fitParSwitch = np.zeros(len(fitParAddrs))
        bupFitPSwitch = copy.deepcopy(fitParSwitch)
        fitParLims = np.zeros((len(fitParAddrs), 2))
        fitParLims.fill(None)
        bupFitParLims = copy.deepcopy(fitParLims)
        parsHist.append(parsCurr)

        # -----------------------------------------------------
        # -------- Explanation of parameter structures --------
        # scattering data loading
        # [series, point, coordinate]

        parsCurr = pars_adjust(parsCurr, init=True)
        bupParsCurr = copy.deepcopy(parsCurr)

        qlims = np.array([expdata[0, :, 0].min(), expdata[0, :, 0].max()])
        bupqlims = np.array(qlims)

        mainwin = SaxMlvWin()
        mainwin.show()

        for i in range(len(expdata)):
            newwin = DataSetWin(ser_nr=i, tit=str(self.dtLines[i].text()))
            fitWins.append(newwin)

        for win in fitWins:
            win.show()

        self.close()

    def pushCancel(self):
        sys.exit()


class MethWin(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MethWin, self).__init__(parent)

        self.setWindowTitle("Method Window")

        self.centralwidget = QtGui.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.mainLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.optBox = QtGui.QGroupBox(self.centralwidget)
        self.optBox.setMinimumWidth(80)
        self.optBox.setTitle("Methods")
        self.optBoxLayout = QtGui.QVBoxLayout(self.optBox)
        self.mainLayout.addWidget(self.optBox)
        self.saxsButton = QtGui.QPushButton(QtCore.QString("SAXS MLV"), self.optBox)
        self.connect(self.saxsButton, QtCore.SIGNAL("clicked()"), self.show_sax_mlv)
        self.saxsButton.setAutoDefault(True)
        self.optBoxLayout.addWidget(self.saxsButton)
        self.testbutton = QtGui.QPushButton(QtCore.QString("For another method"), self.optBox)
        self.connect(self.testbutton, QtCore.SIGNAL("clicked()"), self.show_sax_mlv)
        self.optBoxLayout.addWidget(self.testbutton)

    def show_sax_mlv(self):
        global loadwin
        global metwin

        loadwin.show()
        metwin.close()

    def showSaxMlv(self):
        global mainwin

        mainwin.show()
        self.close()


class DataSetWin(QtGui.QMainWindow):
    def __init__(self, parent=None, ser_nr=0, name="Data Series Nr. ", tit=None):
        super(DataSetWin, self).__init__(parent)
        # TODO add menu option to exchange the loaded data set
        global expdata
        global fitParAddrs
        global fitParLims
        global parsCurr
        global divadm

        QtGui.QShortcut(QtGui.QKeySequence("Ctrl+Q"), self, self.close)
        self.ser_nr = ser_nr
        self.title = name + str(self.ser_nr + 1) + "   (" + tit + ")"
        self.expDt = np.array(expdata[self.ser_nr])

        self.resize(800, 600)

        self.setWindowTitle(QtCore.QString(self.title))
        self.centralwidget = QtGui.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.totalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.menuBar = QtGui.QMenuBar(self.centralwidget)
        self.menuBar.setFixedHeight(22)
        self.menuMain = self.menuBar.addMenu("Main")
        self.generAction = self.menuMain.addAction(QtCore.QString("Generate data"), self.generate_data)
        self.qUpAction = self.menuMain.addAction(QtCore.QString(u"q [\u00c5\u207b\u00b9] \u2192 [nm\u207b\u00b9]"),
                                                 self.qScaleUp)
        self.qDownAction = self.menuMain.addAction(QtCore.QString(u"q [nm\u207b\u00b9] \u2192 [\u00c5\u207b\u00b9]"),
                                                   self.qScaleDown)
        self.menuWins = self.menuBar.addMenu("Windows")
        self.showAction = self.menuWins.addAction(QtCore.QString("Show All"), self.show_all, "Ctrl+A")
        self.totalLayout.addWidget(self.menuBar)
        self.centerLayout = QtGui.QHBoxLayout()
        self.totalLayout.addLayout(self.centerLayout)
        self.leftLayout = QtGui.QVBoxLayout()
        self.centerLayout.addLayout(self.leftLayout)
        self.rightLayout = QtGui.QVBoxLayout()
        self.centerLayout.addLayout(self.rightLayout)

        self.updLines = []   # list of all editline functions needed to be updated
        self.limChks = []   # list of checkboxes en-/disabled due to diff. evol. algorithm
        self.fixHeadChks = []   # list of checkboxes disabled by 'fix-head button'
        self.conHeadChks = []   # list of checkboxes disabled by 'con-head button'
        self.conAdmChks = []    # list of checkboxes disables/enabled by 'conn_adm_on' switch

        # left window part
        self.fitFrame = CurveDialog(edit=False, toolbar=True, wintitle=self.title,
                                    options=dict(title="Fitting curve", xlabel=u"q",
                                                 xunit=u"\xc5<sup>-1</sup>", ylabel="I(q)", yunit=u"arb. u."))
        self.fitFrame.add_tool(LabelTool)

        # fitting plot reference
        self.dataCurve = qmake.error(x=self.expDt[:, 0], y=self.expDt[:, 1], dx=None, dy=self.expDt[:, 2],
                                     color='black', marker='Ellipse', errorbarcap=None, curvestyle='NoCurve',
                                     markeredgecolor='black', markerfacecolor='white', markersize=5, title=u"sc. data")

        self.fitPlot = self.fitFrame.get_plot()
        self.fitPlot.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.fitPlot.set_axis_scale('left', 'log')
        self.fitPlot.add_item(self.dataCurve)
        self.fitData = np.maximum(fitfun(self.expDt[:, 0], parsCurr[self.ser_nr]), 1.0e-10)
        self.fitCurve = qmake.curve(self.expDt[:, 0], self.fitData, title=u"fit curve", color='green',
                                    linewidth=3)
        self.fitFrame.setAutoFillBackground(True)
        self.fitPlot.add_item(self.fitCurve)
        self.qmin = qmake.vcursor(self.expDt[:, 0].min(), label="qmin=%.3f", readonly=True)
        self.qmax = qmake.vcursor(self.expDt[:, 0].max(), label="qmax=%.3f", readonly=True)
        self.fitPlot.add_item(self.qmin)
        self.fitPlot.add_item(self.qmax)
        self.connect(self.fitPlot, guisigs.SIG_MARKER_CHANGED, self.upd_range)
        self.minp = max(min(self.fitData.min(), self.expDt[:, 1].min()) * 0.5, 1.0e-10)
        self.maxp = self.expDt[:, 1].max() * 2.0
        self.fitPlot.set_axis_limits('left', self.minp, self.maxp)
        self.fitPlot.set_axis_limits('bottom', self.expDt[:, 0].min() * 0.9,
                                     self.expDt[:, 0].max() + self.expDt[:, 0].min() * 0.1)

        # residual plot reference
        self.resiFrame = CurveDialog(edit=False, toolbar=False, wintitle=self.title,
                                     options=dict(xlabel=u"q", xunit=u"\xc5<sup>-1</sup>",
                                                  ylabel=u"Res."))
        self.resiPlot = self.resiFrame.get_plot()
        self.resiPlot.setMinimumHeight(300)
        self.resiData = (self.fitData - self.expDt[:, 1]) / self.expDt[:, 2]
        resids = qmake.curve(x=self.expDt[:, 0], y=self.resiData, color='black')
        self.resiPlot.add_item(resids)
        self.rminp = self.resiData.min() * 0.9
        self.rmaxp = self.resiData.max() * 1.1
        self.resiPlot.set_axis_limits('left', self.rminp, self.rmaxp)
        self.resiPlot.set_axis_limits('bottom', self.expDt[:, 0].min() * 0.9,
                                      self.expDt[:, 0].max() + self.expDt[:, 0].min() * 0.1)
        self.plotBox = QtGui.QGroupBox(self.centralwidget)
        self.plotBox.setTitle(QtGui.QApplication.translate(self.title, "Data fit",
                                                           None, QtGui.QApplication.UnicodeUTF8))
        self.plotLayout = QtGui.QVBoxLayout(self.plotBox)
        self.leftLayout.addWidget(self.plotBox)
        self.scbCtrlLayout = QtGui.QHBoxLayout()
        self.leftLayout.addLayout(self.scbCtrlLayout)

        # e-dens plot(s) reference(s)
        self.eDensFrame = CurveDialog(edit=False, toolbar=False,
                                         options=dict(title='Electron density profile(s)',
                                                      xlabel=u"z", xunit=u"\xc5",
                                                      ylabel=u"el. density", yunit=u"\xc5<sup>-3</sup>"))
        self.eDensPlot = self.eDensFrame.get_plot()
        self.eDensPlot.setMinimumHeight(300)

        self.eDensZlim = 0.0
        self.grps = list()
        for i in range(2):
            phs = str(i + 1)
            if phs in parsCurr[self.ser_nr].keys():
                self.grps.append(dict((key, value) for key, value in parsCurr[self.ser_nr][phs].iteritems()
                                 if key != 'Prps'))
                self.eDensZlim = max(densProfLim(self.grps[-1]), self.eDensZlim)
        self.numPlotPoints = 500
        self.colors = ['red', 'blue']
        self.phsNames = [u"L<sub>D</sub>-phase", u"L<sub>O</sub>-phase"]
        for i in range(2):
            phs = str(i + 1)
            if phs in parsCurr[self.ser_nr].keys():
                denscurve = densProfile(lcheck(parsCurr[self.ser_nr][phs]['Prps']['Area']),
                                              self.grps[i], divadm, self.numPlotPoints, self.eDensZlim)
                self.densCurve = qmake.curve(np.linspace(-self.eDensZlim, self.eDensZlim, self.numPlotPoints),
                                             denscurve, title=self.phsNames[i], color=self.colors[i], linewidth=2)
                self.eDensPlot.add_item(self.densCurve)
        self.eDensLegend = qmake.legend("BR")
        self.eDensPlot.add_item(self.eDensLegend)
        self.eDensPlot.set_axis_limits('bottom', -self.eDensZlim - 5.0, self.eDensZlim + 5.0)

        # Group-structure plots
        # TODO write a function yielding a set of curves which will be filled into a group frame
        self.colorList = ['r', 'b', 'm', 'g', 'G', 'c', 'k', 'y']
        self.groupNumPoints = 1000
        z_arr = np.linspace(-self.eDensZlim, self.eDensZlim, self.groupNumPoints)
        self.groupPlots = list()
        for i in range(len(parsCurr[self.ser_nr]) - 1):
            phaseFrame = CurveDialog(edit=False, toolbar=False,
                                     options=dict(title=str(i + 1) + '. phase structure',
                                                  xlabel=u"z", xunit=u"\xc5",
                                                  ylabel=u"Lateral fraction"))
            self.groupPlots.append(phaseFrame.get_plot())
            self.groupPlots[i].setMinimumHeight(250)
            cclr = 0
            for grp in phaseStructure(z_arr, lcheck(parsCurr[self.ser_nr][str(i+1)]['Prps']['Area']), self.grps[i],
                                      divadm):
                self.grpCurve = qmake.curve(z_arr, grp[0], title=grp[1], color=self.colorList[cclr], linewidth=2)
                self.groupPlots[i].add_item(self.grpCurve)
                cclr += 1
            self.phsLegend = qmake.legend("TR")
            self.groupPlots[i].add_item(self.phsLegend)

        self.plotTabs = QtGui.QTabWidget(self.plotBox)
        self.plotLayout.addWidget(self.plotTabs)
        self.fitTab = QtGui.QWidget(self.plotTabs)
        self.fitTabLayout = QtGui.QVBoxLayout(self.fitTab)
        self.fitTabLayout.addWidget(self.fitPlot)
        self.resTab = QtGui.QWidget(self.plotTabs)
        self.resTabLayout = QtGui.QVBoxLayout(self.resTab)
        self.resTabLayout.addWidget(self.resiPlot)
        self.eDensTab = QtGui.QWidget(self.plotTabs)
        self.eDensTabLayout = QtGui.QVBoxLayout(self.eDensTab)
        self.eDensTabLayout.addWidget(self.eDensPlot)
        self.plotTabs.addTab(self.fitTab, "Fit plot")
        self.plotTabs.addTab(self.resTab, "Residuals")
        self.plotTabs.addTab(self.eDensTab, "E-dens. profiles")
        for i in range(len(parsCurr[self.ser_nr]) - 1):
            self.grpTab = QtGui.QWidget(self.plotTabs)
            self.grpTabLayout = QtGui.QVBoxLayout(self.grpTab)
            self.grpTabLayout.addWidget(self.groupPlots[i])
            self.plotTabs.addTab(self.grpTab, str(i + 1) + ". Phs. Groups")

        # Scale/background box
        self.scbBox = QtGui.QGroupBox(self.centralwidget)
        self.scbBox.setTitle(QtGui.QApplication.translate(self.title, "Scale and background",
                                                          None, QtGui.QApplication.UnicodeUTF8))
        self.scbLayout = QtGui.QGridLayout(self.scbBox)
        self.scbLayout.setVerticalSpacing(1)
        self.scbLayout.setColumnStretch(9, 2)

        self.ctrlDrawLayout = QtGui.QVBoxLayout()
        self.scbCtrlLayout.addLayout(self.ctrlDrawLayout)
        self.scbCtrlLayout.addWidget(self.scbBox)

        self.phaseTabs = QtGui.QTabWidget(self.centralwidget)
        self.phaseTabs.setFixedWidth(450)
        self.rightLayout.addWidget(self.phaseTabs)

        self.propBoxes = []  # list of properties groupboxes
        self.propLays = []   # list of properties layouts
        self.grpBoxes = []   # list of group groupboxes
        self.grpLays = []    # list of group layouts

        for i in range(1, len(parsCurr[self.ser_nr])):
            self.phsTab = QtGui.QWidget(self.phaseTabs)
            self.phsLayout = QtGui.QVBoxLayout(self.phsTab)
            self.phaseTabs.addTab(self.phsTab, str(i) + ". Phase")
                # properties box
            self.propBox = QtGui.QGroupBox(self.phsTab)
            self.propBox.setTitle(QtGui.QApplication.translate(self.title, "Properties",
                                                                None, QtGui.QApplication.UnicodeUTF8))
            self.propLayout = QtGui.QGridLayout(self.propBox)
            self.phsLayout.addWidget(self.propBox)
            self.propLayout.setVerticalSpacing(1)
            self.propLayout.setColumnStretch(12, 20)
            self.propBoxes.append(self.propBox)
            self.propLays.append(self.propLayout)
                # groups box
            self.groupBox = QtGui.QGroupBox(self.phsTab)
            self.groupBox.setTitle(QtGui.QApplication.translate(self.title, "Groups",
                                                                 None, QtGui.QApplication.UnicodeUTF8))
            self.groupLayout = QtGui.QGridLayout(self.groupBox)
            self.groupLayout.setVerticalSpacing(1)
            self.groupLayout.setColumnStretch(9, 2)
            self.phsLayout.addWidget(self.groupBox)
            self.grpBoxes.append(self.groupBox)
            self.grpLays.append(self.groupLayout)

        # ordering list of the phase parameters
        parord = ['AsymFr', 'Frct', 'Purity', 'Nbil', 'Nsig', 'Dsp', 'Eta', 'Ndiff', 'Area', 'vol', 'dens']

        phsswitch = None
        self.polyDispSigChks = []
        self.regimeDisables = []
        for i in range(len(fitParAddrs)):
            if fitParAddrs[i][0] == self.ser_nr:
                if phsswitch != fitParAddrs[i][1]:
                    k = 0
                    phsswitch = copy.deepcopy(fitParAddrs[i][1])
                if fitParAddrs[i][1] == '0':
                    if fitParAddrs[i][3] == 'Scale':
                        self.label = QtGui.QLabel(QtCore.QString("Scale"), self.scbBox)
                        self.label.setFixedWidth(60)
                        self.scbLayout.addWidget(self.label, k, 0)
                        self.chkBox = QtGui.QCheckBox(self.scbBox)
                        self.chkBox.setObjectName(QtCore.QString("Scale_ser" + str(self.ser_nr + 1)))
                    if fitParAddrs[i][3] == 'Bckg':
                        self.label = QtGui.QLabel(QtCore.QString("BgPar." + str(fitParAddrs[i][4] + 1)), self.scbBox)
                        self.label.setFixedWidth(60)
                        self.scbLayout.addWidget(self.label, k, 0)
                        self.chkBox = QtGui.QCheckBox(self.scbBox)
                        self.chkBox.setObjectName(QtCore.QString(str(fitParAddrs[i][0])
                                                                 + "_ser_BgPr_" + str(fitParAddrs[i][4])))
                    self.connect(self.chkBox, QtCore.SIGNAL("stateChanged(int)"),
                                 partial(self.chng_st, self.chkBox, i))
                    self.connect(self.chkBox, QtCore.SIGNAL("stateChanged(int)"),
                                 self.set_lim_chks)
                    self.scbLayout.addWidget(self.chkBox, k, 1)
                    self.line = QtGui.QLineEdit("{0:.3g}".format(getFromDict(parsCurr, fitParAddrs[i] + [0]),
                                                self.scbBox))
                    self.line.setFixedWidth(50)
                    self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                 partial(self.cpval, self.line, 'val', i))
                    self.updLines.append(partial(self.upval, self.line, 'val', i))
                    self.scbLayout.addWidget(self.line, k, 2)
                    self.tag = QtGui.QLabel(QtCore.QString("+ -"), self.scbBox)
                    self.scbLayout.addWidget(self.tag, k, 3)
                    self.line = QtGui.QLineEdit("{0:.3g}".format(getFromDict(parsCurr, fitParAddrs[i] + [1])),
                                                self.scbBox)
                    self.line.setFixedWidth(50)
                    self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                 partial(self.cpval, self.line, 'err', i))
                    self.updLines.append(partial(self.upval, self.line, 'err', i))
                    self.scbLayout.addWidget(self.line, k, 4)
                    self.chkBox = QtGui.QCheckBox(QtCore.QString("Limits:"), self.scbBox)
                    self.limChks.append(self.chkBox)
                    self.scbLayout.addWidget(self.chkBox, k, 5)
                    self.limDis = []
                    self.line = QtGui.QLineEdit("{0:.3g}".format(fitParLims[i][0]),
                                                self.scbBox)
                    self.line.setFixedWidth(50)
                    self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                 partial(self.cpval, self.line, 'lim_l', i))
                    self.updLines.append(partial(self.upval, self.line, 'lim_l', i))
                    self.line.setDisabled(True)
                    self.limDis.append(self.line)
                    self.scbLayout.addWidget(self.line, k, 6)
                    self.tag = QtGui.QLabel(QtCore.QString("to"), self.scbBox)
                    self.scbLayout.addWidget(self.tag, k, 7)
                    self.line = QtGui.QLineEdit("{0:.3g}".format(fitParLims[i][1]),
                                                self.scbBox)
                    self.line.setFixedWidth(50)
                    self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                 partial(self.cpval, self.line, 'lim_r', i))
                    self.updLines.append(partial(self.upval, self.line, 'lim_r', i))
                    self.line.setDisabled(True)
                    self.limDis.append(self.line)
                    self.scbLayout.addWidget(self.line, k, 8)
                    self.connect(self.chkBox, QtCore.SIGNAL("stateChanged(int)"),
                                 partial(self.chng_st_lim, self.chkBox, self.limDis, i))
                    k += 1
                else:
                    if fitParAddrs[i][2] in ['Prps', 'Chains']:
                        lokidx = parord.index(fitParAddrs[i][3])
                        if fitParAddrs[i][2] == 'Chains':
                            self.label = QtGui.QLabel(QtCore.QString("Chains_" + fitParAddrs[i][3]),
                                                  self.propBoxes[int(fitParAddrs[i][1]) - 1])
                        else:
                            self.label = QtGui.QLabel(QtCore.QString(fitParAddrs[i][3]),
                                                  self.propBoxes[int(fitParAddrs[i][1]) - 1])
                        self.label.setFixedWidth(60)
                        self.propLays[int(fitParAddrs[i][1]) - 1].addWidget(self.label,
                                                                            lokidx, 0)
                        self.chkBox = QtGui.QCheckBox(self.propBoxes[int(fitParAddrs[i][1]) - 1])
                        self.chkBox.setObjectName(QtCore.QString(str(fitParAddrs[i][0]) + fitParAddrs[i][3] +
                                                                 fitParAddrs[i][1]))
                        self.connect(self.chkBox, QtCore.SIGNAL("stateChanged(int)"),
                                     partial(self.chng_st, self.chkBox, i))
                        self.connect(self.chkBox, QtCore.SIGNAL("stateChanged(int)"),
                                     self.set_lim_chks)
                        if fitParAddrs[i][3] == 'Nsig':
                            self.chkBox.setDisabled(True)
                            self.polyDispSigChks.append(self.chkBox)
                        if fitParAddrs[i][1] == '2' and fitParAddrs[i][3] in ['Eta', 'Dsp', 'Nbil', 'Nsig', 'Ndiff']:
                            self.regimeDisables.append(self.chkBox)
                        self.propLays[int(fitParAddrs[i][1]) - 1].addWidget(self.chkBox, lokidx, 1)
                        self.line = QtGui.QLineEdit("{0:.3g}".format(getFromDict(parsCurr, fitParAddrs[i] + [0])),
                                                    self.propBoxes[int(fitParAddrs[i][1]) - 1])
                        self.line.setFixedWidth(50)
                        if fitParAddrs[i][1] == '2' and fitParAddrs[i][3] in ['Eta', 'Dsp', 'Nbil', 'Nsig', 'Ndiff']:
                            self.regimeDisables.append(self.line)
                        self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                     partial(self.cpval, self.line, 'val', i))
                        self.updLines.append(partial(self.upval, self.line, 'val', i))
                        self.propLays[int(fitParAddrs[i][1]) - 1].addWidget(self.line, lokidx, 2)
                        self.tag = QtGui.QLabel(QtCore.QString("+ -"), self.propBoxes[int(fitParAddrs[i][1]) - 1])
                        self.propLays[int(fitParAddrs[i][1]) - 1].addWidget(self.tag, lokidx, 3)
                        self.line = QtGui.QLineEdit("{0:.3g}".format(getFromDict(parsCurr, fitParAddrs[i] + [1])),
                                                    self.propBoxes[int(fitParAddrs[i][1]) - 1])
                        self.line.setFixedWidth(50)
                        if fitParAddrs[i][1] == '2' and fitParAddrs[i][3] in ['Eta', 'Dsp', 'Nbil', 'Nsig', 'Ndiff']:
                            self.regimeDisables.append(self.line)
                        self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                     partial(self.cpval, self.line, 'err', i))
                        self.updLines.append(partial(self.upval, self.line, 'err', i))
                        self.propLays[int(fitParAddrs[i][1]) - 1].addWidget(self.line, lokidx, 4)
                        self.chkBox = QtGui.QCheckBox(QtCore.QString("Limits:"),
                                                      self.propBoxes[int(fitParAddrs[i][1]) - 1])
                        self.limChks.append(self.chkBox)
                        if fitParAddrs[i][1] == '2' and fitParAddrs[i][3] in ['Eta', 'Dsp', 'Nbil', 'Nsig', 'Ndiff']:
                            self.regimeDisables.append(self.chkBox)
                        self.propLays[int(fitParAddrs[i][1]) - 1].addWidget(self.chkBox, lokidx, 5)
                        self.limDis = []
                        self.line = QtGui.QLineEdit("{0:.3g}".format(fitParLims[i][0]),
                                                    self.propBoxes[int(fitParAddrs[i][1]) - 1])
                        self.line.setFixedWidth(50)
                        if fitParAddrs[i][1] == '2' and fitParAddrs[i][3] in ['Eta', 'Dsp', 'Nbil', 'Nsig', 'Ndiff']:
                            self.regimeDisables.append(self.line)
                        self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                     partial(self.cpval, self.line, 'lim_l', i))
                        self.updLines.append(partial(self.upval, self.line, 'lim_l', i))
                        self.line.setDisabled(True)
                        self.limDis.append(self.line)
                        self.propLays[int(fitParAddrs[i][1]) - 1].addWidget(self.line, lokidx, 6)
                        self.tag = QtGui.QLabel(QtCore.QString("to"),
                                                self.propBoxes[int(fitParAddrs[i][1]) - 1])
                        self.propLays[int(fitParAddrs[i][1]) - 1].addWidget(self.tag, lokidx, 7)
                        self.line = QtGui.QLineEdit("{0:.3g}".format(fitParLims[i][1]),
                                                    self.propBoxes[int(fitParAddrs[i][1]) - 1])
                        self.line.setFixedWidth(50)
                        if fitParAddrs[i][1] == '2' and fitParAddrs[i][3] in ['Eta', 'Dsp', 'Nbil', 'Nsig', 'Ndiff']:
                            self.regimeDisables.append(self.line)
                        self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                     partial(self.cpval, self.line, 'lim_r', i))
                        self.updLines.append(partial(self.upval, self.line, 'lim_r', i))
                        self.line.setDisabled(True)
                        self.limDis.append(self.line)
                        self.connect(self.chkBox, QtCore.SIGNAL("stateChanged(int)"),
                                     partial(self.chng_st_lim, self.chkBox, self.limDis, i))
                        self.propLays[int(fitParAddrs[i][1]) - 1].addWidget(self.line, lokidx, 8)
                    else:
                        self.label = QtGui.QLabel(QtCore.QString(fitParAddrs[i][2] + "_" + fitParAddrs[i][3]),
                                                  self.grpBoxes[int(fitParAddrs[i][1]) - 1])
                        self.label.setFixedWidth(60)
                        self.grpLays[int(fitParAddrs[i][1]) - 1].addWidget(self.label, k, 0)
                        self.chkBox = QtGui.QCheckBox(self.grpBoxes[int(fitParAddrs[i][1]) - 1])
                        self.chkBox.setObjectName(QtCore.QString(str(fitParAddrs[i][0]) + fitParAddrs[i][3] +
                                                                 fitParAddrs[i][1]))
                        self.connect(self.chkBox, QtCore.SIGNAL("stateChanged(int)"),
                                     partial(self.chng_st, self.chkBox, i))
                        self.connect(self.chkBox, QtCore.SIGNAL("stateChanged(int)"), self.set_lim_chks)
                        if fitParAddrs[i][1] == '2' and fitParAddrs[i][3] == 'dc':
                            self.chkBox.setDisabled(True)
                        if fitParAddrs[i][2] == connkeys[self.ser_nr][int(fitParAddrs[i][1])] and \
                                fitParAddrs[i][3] == 'z':
                            self.conHeadChks.append(self.chkBox)
                        if getFromDict(parsCurr, fitParAddrs[i][:-1] + ['id']) == 0 \
                                and getFromDict(parsCurr, fitParAddrs[i][:-1] + ['env']) == 'w':
                            if fitParAddrs[i][2] != connkeys[self.ser_nr][int(fitParAddrs[i][1])] and \
                                    fitParAddrs[i][3] == 'z':
                                self.fixHeadChks.append(self.chkBox)
                        if getFromDict(parsCurr, fitParAddrs[i][:-1] + ['id']) == 1 and fitParAddrs[i][3] == 'z':
                            self.conAdmChks.append(self.chkBox)
                        self.grpLays[int(fitParAddrs[i][1]) - 1].addWidget(self.chkBox, k, 1)
                        self.line = QtGui.QLineEdit("{0:.3g}".format(getFromDict(parsCurr, fitParAddrs[i] + [0]),
                                                    self.grpBoxes[int(fitParAddrs[i][1]) - 1]))
                        self.line.setFixedWidth(50)
                        self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                     partial(self.cpval, self.line, 'val', i))
                        self.updLines.append(partial(self.upval, self.line, 'val', i))
                        self.grpLays[int(fitParAddrs[i][1]) - 1].addWidget(self.line, k, 2)
                        self.tag = QtGui.QLabel(QtCore.QString("+ -"), self.grpBoxes[int(fitParAddrs[i][1]) - 1])
                        self.grpLays[int(fitParAddrs[i][1]) - 1].addWidget(self.tag, k, 3)
                        self.line = QtGui.QLineEdit("{0:.3g}".format(getFromDict(parsCurr, fitParAddrs[i] + [1])),
                                                    self.grpBoxes[int(fitParAddrs[i][1]) - 1])
                        self.line.setFixedWidth(50)
                        self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                     partial(self.cpval, self.line, 'err', i))
                        self.updLines.append(partial(self.upval, self.line, 'err', i))
                        self.grpLays[int(fitParAddrs[i][1]) - 1].addWidget(self.line, k, 4)
                        self.chkBox = QtGui.QCheckBox(QtCore.QString("Limits:"),
                                                      self.grpBoxes[int(fitParAddrs[i][1]) - 1])
                        self.limChks.append(self.chkBox)
                        self.grpLays[int(fitParAddrs[i][1]) - 1].addWidget(self.chkBox, k, 5)
                        self.limDis = []
                        self.line = QtGui.QLineEdit("{0:.3g}".format(fitParLims[i][0]),
                                                    self.grpBoxes[int(fitParAddrs[i][1]) - 1])
                        self.line.setFixedWidth(50)
                        self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                     partial(self.cpval, self.line, 'lim_l', i))
                        self.updLines.append(partial(self.upval, self.line, 'lim_l', i))
                        self.line.setDisabled(True)
                        self.limDis.append(self.line)
                        self.grpLays[int(fitParAddrs[i][1]) - 1].addWidget(self.line, k, 6)
                        self.tag = QtGui.QLabel(QtCore.QString("to"),
                                                self.grpBoxes[int(fitParAddrs[i][1]) - 1])
                        self.grpLays[int(fitParAddrs[i][1]) - 1].addWidget(self.tag, k, 7)
                        self.line = QtGui.QLineEdit("{0:.3g}".format(fitParLims[i][1]),
                                                    self.grpBoxes[int(fitParAddrs[i][1]) - 1])
                        self.line.setFixedWidth(50)
                        self.connect(self.line, QtCore.SIGNAL("editingFinished()"),
                                     partial(self.cpval, self.line, 'lim_r', i))
                        self.updLines.append(partial(self.upval, self.line, 'lim_r', i))
                        self.line.setDisabled(True)
                        self.limDis.append(self.line)
                        self.connect(self.chkBox, QtCore.SIGNAL("stateChanged(int)"),
                                     partial(self.chng_st_lim, self.chkBox, self.limDis, i))
                        self.grpLays[int(fitParAddrs[i][1]) - 1].addWidget(self.line, k, 8)
                        k += 1

        self.molCtrlBox = QtGui.QGroupBox(self.centralwidget)
        self.molCtrlBox.setTitle(QtGui.QApplication.translate(self.title, "Molecular structure",
                                                          None, QtGui.QApplication.UnicodeUTF8))
        self.molCtrlLayout = QtGui.QGridLayout(self.molCtrlBox)
        self.conHeadChk = QtGui.QCheckBox(QtCore.QString("Connect HC-core"), self.molCtrlBox)
        self.connect(self.conHeadChk, QtCore.SIGNAL("stateChanged(int)"), partial(self.con_heads,
                                                                         self.conHeadChk))
        self.molCtrlLayout.addWidget(self.conHeadChk, 0, 0)
        self.fixHeadChk = QtGui.QCheckBox(QtCore.QString("Connect headgroups"), self.molCtrlBox)
        self.connect(self.fixHeadChk, QtCore.SIGNAL("stateChanged(int)"), partial(self.fix_heads,
                                                                        self.fixHeadChk))
        self.molCtrlLayout.addWidget(self.fixHeadChk, 0, 1)
        self.divAdmChk = QtGui.QCheckBox(QtCore.QString("Divide admixture"))
        self.connect(self.divAdmChk, QtCore.SIGNAL("stateChanged(int)"), self.div_adm)
        self.molCtrlLayout.addWidget(self.divAdmChk, 0, 2)
        self.conAdmChk = QtGui.QCheckBox(u"Connect admixture")
        self.connect(self.conAdmChk, QtCore.SIGNAL("stateChanged(int)"), partial(self.con_adm,
                                                                                 self.conAdmChk))
        self.molCtrlLayout.addWidget(self.conAdmChk, 1, 0)
        self.ctrlDrawLayout.addWidget(self.molCtrlBox)
        self.drawBox = QtGui.QGroupBox(self.centralwidget)
        self.drawBox.setTitle(QtGui.QApplication.translate(self.title, "Plot controls",
                                                          None, QtGui.QApplication.UnicodeUTF8))
        self.drawLayout = QtGui.QHBoxLayout(self.drawBox)
        self.rdrwButt = QtGui.QPushButton(QtCore.QString("Redraw"), self.drawBox)
        self.connect(self.rdrwButt, QtCore.SIGNAL("clicked()"), self.plot_update)
        self.drawLayout.addWidget(self.rdrwButt)
        self.clrButt = QtGui.QPushButton(QtCore.QString("Clear"), self.drawBox)
        self.connect(self.clrButt, QtCore.SIGNAL("clicked()"), self.clear_plots)
        self.drawLayout.addWidget(self.clrButt)
        self.ctrlDrawLayout.addWidget(self.drawBox)

    def clear_plots(self):
        self.fitPlot.del_all_items(True)
        self.fitPlot.replot()
        self.resiPlot.del_all_items(True)
        self.resiPlot.replot()
        self.eDensPlot.del_all_items(True)
        self.eDensPlot.replot()
        for plot in self.groupPlots:
            plot.del_all_items(True)
            plot.replot()

    def plot_update(self):
        """
        update the plots
        :return: NULL
        """
        global parsCurr
        global fitalg
        global expdata
        global qlims
        global divadm

        parsCurr = pars_adjust(parsCurr)
        self.updBox()

        fitalg = 0

        self.clear_plots()
        self.dataCurve = qmake.error(x=self.expDt[:, 0], y=self.expDt[:, 1], dx=None, dy=self.expDt[:, 2],
                                 color='black', marker='Ellipse', errorbarcap=None, curvestyle='NoCurve',
                                 markeredgecolor='black', markerfacecolor='white', markersize=5, title=u"sc. data")
        self.fitPlot.add_item(self.dataCurve)
        self.fitData = np.maximum(fitfun(self.expDt[:, 0], parsCurr[self.ser_nr]), 1.0e-10)
        self.fitCurve = qmake.curve(self.expDt[:, 0], self.fitData, title=u"fit curve", color='green',
                               linewidth=3)
        self.fitPlot.add_item(self.fitCurve)
        self.qmin = qmake.vcursor(qlims[0], label="qmin=%.3f", readonly=True)
        self.qmax = qmake.vcursor(qlims[1], label="qmax=%.3f", readonly=True)
        self.fitPlot.add_item(self.qmin)
        self.fitPlot.add_item(self.qmax)
        self.minp = max(min(self.fitData.min(), self.expDt[:, 1].min()) * 0.5, 1.0e-10)
        self.maxp = self.expDt[:, 1].max() * 2.0
        self.fitPlot.set_axis_limits('left', self.minp, self.maxp)
        self.fitPlot.set_axis_limits('bottom', self.expDt[:, 0].min() * 0.9,
                                     self.expDt[:, 0].max() + self.expDt[:, 0].min() * 0.1)
        self.fitPlot.replot()

        self.resiData = (self.fitData - self.expDt[:, 1]) / self.expDt[:, 2]
        resids = qmake.curve(x=self.expDt[:, 0], y=self.resiData, color='black')
        self.resiPlot.add_item(resids)
        self.rMinp = self.resiData.min() * 0.9
        self.rMaxp = self.resiData.max() * 1.1
        self.resiPlot.set_axis_limits('left', self.rMinp, self.rMaxp)
        self.resiPlot.set_axis_limits('bottom', self.expDt[:, 0].min() * 0.9,
                                   self.expDt[:, 0].max() + self.expDt[:, 0].min() * 0.1)
        self.resiPlot.replot()

        self.eDensZlim = 0.0
        self.grps = list()
        for i in range(2):
            phs = str(i + 1)
            if phs in parsCurr[self.ser_nr].keys():
                self.grps.append(dict((key, value) for key, value in parsCurr[self.ser_nr][phs].iteritems()
                                 if key != 'Prps'))
                self.eDensZlim = max(densProfLim(self.grps[-1]), self.eDensZlim)
        for i in range(2):
            phs = str(i + 1)
            if phs in parsCurr[self.ser_nr].keys():
                denscurve = densProfile(lcheck(parsCurr[self.ser_nr][phs]['Prps']['Area']),
                                              self.grps[i], divadm, self.numPlotPoints, self.eDensZlim)
                self.densCurve = qmake.curve(np.linspace(-self.eDensZlim, self.eDensZlim, self.numPlotPoints),
                                             denscurve, title=self.phsNames[i], color=self.colors[i], linewidth=2)
                self.eDensPlot.add_item(self.densCurve)
        self.eDensLegend = qmake.legend("BR")
        self.eDensPlot.add_item(self.eDensLegend)
        self.eDensPlot.set_axis_limits('bottom', -self.eDensZlim - 5.0, self.eDensZlim + 5.0)
        self.eDensPlot.replot()

        z_arr = np.linspace(-self.eDensZlim, self.eDensZlim, self.groupNumPoints)
        for i in range(len(parsCurr[self.ser_nr]) - 1):
            cclr = 0
            for grp in phaseStructure(z_arr, lcheck(parsCurr[self.ser_nr][str(i+1)]['Prps']['Area']), self.grps[i],
                                      divadm):
                self.grpCurve = qmake.curve(z_arr, grp[0], title=grp[1], color=self.colorList[cclr], linewidth=2)
                self.groupPlots[i].add_item(self.grpCurve)
                cclr += 1
            self.phsLegend = qmake.legend("TR")
            self.groupPlots[i].add_item(self.phsLegend)
            self.groupPlots[i].replot()

    def upd_range(self):
        global qlims
        global mainwin
        global fitWins
        # TODO after defining qeditlines fot q-range borders insert their update here

        qlims[0] = float(self.qmin.get_pos()[0])
        qlims[1] = float(self.qmax.get_pos()[0])
        mainwin.qMinLine.setText("{0:.3g}".format(qlims[0]))
        mainwin.qMaxLine.setText("{0:.3g}".format(qlims[1]))
        for i in range(len(fitWins)):
            if i != self.ser_nr:
                fitWins[i].plot_update()

    def cpval(self, line, parset, index):
        """
        copies a value inserted into 'line' into an appropriate field indexed by 'index'
        of a parameter array
        :param line: QtGui.QLineEdit
        :param parset: str
        :param index: int
        :return: NULL
        """
        global parsCurr
        global fitParLims

        try:
            val = float(eval(str(line.text())))
            if parset == 'val':
                setInDict(parsCurr, fitParAddrs[index] + [0], val)
            if parset == 'err':
                setInDict(parsCurr, fitParAddrs[index] + [1], val)
            if parset == 'lim_l':
                fitParLims[index][0] = float(val)
            if parset == 'lim_r':
                fitParLims[index][1] = float(val)
        except IOError:
            line.selectAll()
            mainwin.textBox.append("<font color='red'>Invalid value.</font>")
            mainwin.showNormal()    # added to raise the window if minimalized
            mainwin.raise_()        # to bring the main window to the front
        except NameError:
            line.selectAll()
            mainwin.textBox.append("<font color='red'>Invalid value.</font>")
            mainwin.showNormal()    # added to raise the window if minimalized
            mainwin.raise_()        # to bring the main window to the front

    def upval(self, line, parset, index):
        global parsCurr
        global fitParLims

        if parset == 'val':
            txt = "{0:.3g}".format(getFromDict(parsCurr, fitParAddrs[index] + [0]))
        if parset == 'err':
            txt = "{0:.3g}".format(getFromDict(parsCurr, fitParAddrs[index] + [1]))
        if parset == 'lim_l':
            txt = "{0:.3g}".format(fitParLims[index][0])
        if parset == 'lim_r':
            txt = "{0:.3g}".format(fitParLims[index][1])
        line.setText(txt)

    def updBox(self):
        """
        update all values in the editable fields according to an actual mod_pars_curr array
        :return: NULL
        """
        for line in self.updLines:
            line()

    def thrd_updt(self):
        """
        function updating values and plot after n emission of an update signal from a fitting thread
        :return: NULL
        """
        self.updBox()
        self.plot_update()

    def show_all(self):
        """
        show all hidden/closed fit windows
        :return: NULL
        """
        global fitWins
        global mainwin

        mainwin.show()
        for win in fitWins:
            win.show()

    def chng_st(self, chk, index):
        """
        according to the state of a checkbox the function changes values in fitParSwitch
        :param chk: watched QtGui.QCheckBox
        :param field: position fitParSwitch array
        :return: NULL
        """
        global fitParSwitch

        if chk.isChecked():
            fitParSwitch[index] = 1
        if not chk.isChecked():
            fitParSwitch[index] = 0

    def set_lim_chks(self):
        """
        function to check limit checks in the case that diffevo_ch is checked
        :return: NULL
        """
        global fitWins
        global fitParSwitch
        global mainwin

        if mainwin.difChkBox.isChecked():
            for i in range(len(self.limChks)):
                self.limChks[i].setChecked(bool(fitParSwitch[i]))

    def chng_st_lim(self, chk, lineDis, index):
        """
        function to change the state of a sci_back limit array according to the state of checkbox
        :param line_dis: lineedits to be disabled/enabled
        :return: NULL
        """
        global fitParLims
        global mainwin

        for line in lineDis:
            line.setDisabled(not chk.isChecked())

        if chk.isChecked():
            for i in range(2):
                try:
                    fitParLims[index][i] = float(eval(str(lineDis[i].text())))
                except IOError:
                    if str(lineDis[i].text()) == 'nan':
                        pass
                    else:
                        lineDis[i].selectAll()
                        mainwin.textBox.append("<font color='red'>Invalid value.</font>")
                        mainwin.showNormal()    # added to raise the window if minimalized
                        mainwin.raise_()        # to bring the main window to the front
                except NameError:
                    if str(lineDis[i].text()) == 'nan':
                        pass
                    else:
                        lineDis[i].selectAll()
                        mainwin.textBox.append("<font color='red'>Invalid value.</font>")
                        mainwin.showNormal()    # added to raise the window if minimalized
                        mainwin.raise_()        # to bring the main window to the front
        if not chk.isChecked():
            for i in range(2):
                fitParLims[index][i] = None

    def con_heads(self, chk):
        """
        if checked, the relative positions of only(!) headgroup component groups with regard to the hc_borders
        are preserved:
        :return:
        """
        global connheadson
        global fitWins

        connheadson = bool(chk.isChecked())

        for chk in self.conHeadChks:
            if connheadson:
                chk.setChecked(False)
            chk.setDisabled(connheadson)

        for i in range(len(fitWins)):
            if i != self.ser_nr:
                fitWins[i].conHeadChk.setChecked(connheadson)

    def con_adm(self, chk):
        """
        A switch to (un-)keep the relative positions of admixtures to the HC-border fixed.
        :param chk: a corresponding checkbox in the gui
        :return: NULL
        """
        global conn_adm_on
        global fitWins

        conn_adm_on = bool(chk.isChecked())

        for chk in self.conAdmChks:
            if conn_adm_on:
                chk.setChecked(False)
            chk.setDisabled(conn_adm_on)

        for i in range(len(fitWins)):
            if i != self.ser_nr:
                fitWins[i].conAdmChk.setChecked(conn_adm_on)

    def fix_heads(self, chk):
        """
        if checked, the mutual distances of component groups in polar headgroups remain constant
        :param chk: corresponding checkbox in the gui
        :return: NULL
        """
        global fixheadson

        fixheadson = bool(chk.isChecked())

        for chk in self.fixHeadChks:
            if fixheadson:
                chk.setChecked(False)
            chk.setDisabled(fixheadson)

        for i in range(len(fitWins)):
            if i != self.ser_nr:
                fitWins[i].fixHeadChk.setChecked(fixheadson)

    def div_adm(self):
        global divadm
        global fitWins

        divadm = bool(self.divAdmChk.isChecked())

        for i in range(len(fitWins)):
            if i != self.ser_nr:
                fitWins[i].divAdmChk.setChecked(divadm)

    def qScaleUp(self):
        """
        multiplies q-interval by 10
        :return: NULL
        """
        global expdata

        expdata[self.ser_nr][:, 0] *= 10.0
        self.expDt[:, 0] *= 10.0

        self.plot_update()

    def qScaleDown(self):
        """
        multiplies q-interval by 0.1
        :return: NULL
        """
        global expdata

        expdata[self.ser_nr][:, 0] *= 0.1
        self.expDt[:, 0] *= 0.1

        self.plot_update()

    def generate_data(self):
        """
        function to generate artificial data set based on loaded exp. data
        q's and corresponding relative uncertainties are the same. as an input the multiple of an uncertainty
        interval for data noise spread, is prompted
        :return: a data file
        """
        global expdata

        self.params = GenerParams()
        if self.params.edit():
            self.outdata = np.array(self.expDt)
            self.relerrs = self.expDt[:, 2] / self.expDt[:, 1]

            distarr = None
            if self.params.dist == 0:
                distarr = 2.0 * self.params.spread * (0.5 - np.random.random(len(self.expDt)))
            if self.params.dist == 1:
                distarr = self.params.spread * np.random.randn(len(self.expDt))

            self.outdata[:, 1] = self.fitData * (1.0 + distarr * self.relerrs)
            self.outdata[:, 2] = self.outdata[:, 1] * self.relerrs

            self.expDt = np.array(self.outdata)
            expdata[self.ser_nr] = np.array(self.outdata)
            self.plot_update()
        else:
            pass


class GenerParams(dt.DataSet):
    """
    Data generation parameters
    """
    spread = di.FloatItem("Spread", min=0.0, default=1.0)
    dist = di.ChoiceItem("Spread distribution", ("uniform", "gaussian"))


class Densities(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Densities, self).__init__(parent)
        global parsCurr

        self.setWindowTitle(QtCore.QString("Densities setting"))
        self.centralWidget = QtGui.QWidget(self)
        self.setCentralWidget(self.centralWidget)
        self.centralLayout = QtGui.QVBoxLayout(self.centralWidget)

        self.waterLabel = QtGui.QLabel(QtCore.QString("Water density:  "), self.centralWidget)
        self.waterLine = QtGui.QLineEdit(QtCore.QString(str(dens_w)), self.centralWidget)
        self.waterLine.setFixedWidth(75)
        self.waterLayout = QtGui.QGridLayout()
        self.centralLayout.addLayout(self.waterLayout)
        self.waterLayout.addWidget(self.waterLabel, 0, 0)
        self.waterLayout.addWidget(self.waterLine, 0, 1)
        self.waterLayout.setColumnStretch(2, 12)
        self.carbLines = []
        for i in range(len(parsCurr)):   # series
            self.carbLines.append([])
            self.carbBox = QtGui.QGroupBox("SerNr. " + str(i + 1), self.centralWidget)
            self.centralLayout.addWidget(self.carbBox)
            self.carbLayout = QtGui.QGridLayout(self.carbBox)
            for j in range(1, len(parsCurr[i])):   # phases
                self.hydCarbLab = QtGui.QLabel(QtCore.QString("Hydrocarb" + str(j) + " density: "),
                                               self.carbBox)
                self.hydCarbLine = QtGui.QLineEdit(QtCore.QString(
                    str(getFromDict(parsCurr, [i, str(j), "Chains", "dens"]))),
                    self.carbBox)
                self.hydCarbLine.setFixedWidth(75)
                self.carbLines[-1].append(self.hydCarbLine)
                self.carbLayout.addWidget(self.hydCarbLab, j - 1, 0)
                self.carbLayout.addWidget(self.hydCarbLine, j - 1, 1)
                self.carbLayout.setColumnStretch(5, 5)
                self.carbLayout.setRowStretch(5, 5)

        self.buttLayout = QtGui.QHBoxLayout()
        self.centralLayout.addLayout(self.buttLayout)
        self.okButton = QtGui.QPushButton("OK", self.centralWidget)
        self.connect(self.okButton, QtCore.SIGNAL("clicked()"), self.pushOK)
        self.cancelButton = QtGui.QPushButton("Cancel", self.centralWidget)
        self.connect(self.cancelButton, QtCore.SIGNAL("clicked()"), self.pushCancel)
        self.buttLayout.addWidget(self.okButton)
        self.buttLayout.addWidget(self.cancelButton)
        self.buttLayout.setAlignment(QtCore.Qt.AlignRight)

    def pushOK(self):
        global parsCurr
        global parsHist
        global dens_w

        dens_w = float(self.waterLine.text())

        for i in range(len(parsCurr)):
            for j in range(1, len(parsCurr[i])):
                parsCurr[i][str(j)]['Chains']['dens'][0] = float(self.carbLines[i][j - 1].text())
                for idhist in range(len(parsHist)):
                    parsHist[idhist][i][str(j)]['Chains']['dens'][0] = \
                        float(self.carbLines[i][j - 1].text())

        self.close()

    def pushCancel(self):
        self.close()


class SaxMlvWin(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(SaxMlvWin, self).__init__(parent)
        global qlims
        QtGui.QShortcut(QtGui.QKeySequence("Ctrl+Q"), self, self.close)

        self.devAlg = False
        self.minAlg = False

        self.resFit = []

        self.fitThrd = None

        # self.resize(800, 600)

        self.setWindowTitle(QtCore.QString("SAXS MLVs"))
        self.centralwidget = QtGui.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.menuBar = QtGui.QMenuBar(self.centralwidget)
        self.menuBar.setFixedHeight(22)
        self.menuMain = self.menuBar.addMenu(QtCore.QString("Main"))
        self.saveAction = self.menuMain.addAction(QtCore.QString("Save progress..."), self.save_progress)
        self.loadAction = self.menuMain.addAction(QtCore.QString("Load progress..."), self.load_progress)
        self.expParsAction = self.menuMain.addAction(QtCore.QString("Export parameters..."), self.export_pars)
        self.menuWins = self.menuBar.addMenu(QtCore.QString("Windows"))
        self.showAction = self.menuWins.addAction(QtCore.QString("Show All"), self.show_all, "Ctrl+A")
        self.menuWins.addSeparator()
        self.testAction = self.menuWins.addAction(QtCore.QString("Densities"),
                                                  self.set_densities, "Ctrl+D")

        self.centralLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.centralLayout.addWidget(self.menuBar)

        self.compDisable = []

        self.textBox = QtGui.QTextEdit(self.centralwidget)
        self.textBox.setReadOnly(True)
        self.centralLayout.addWidget(self.textBox)

        self.optBox = QtGui.QGroupBox(self.centralwidget)
        self.optBox.setTitle(QtCore.QString("Options"))
        self.optLayout = QtGui.QVBoxLayout(self.optBox)
        self.centralLayout.addWidget(self.optBox)

        self.strucLayout = QtGui.QHBoxLayout()
        self.strucLayout.setAlignment(QtCore.Qt.AlignLeft)
        self.fitLayout = QtGui.QHBoxLayout()
        self.fitLayout.setAlignment(QtCore.Qt.AlignLeft)
        self.optLayout.addLayout(self.strucLayout)
        self.optLayout.addLayout(self.fitLayout)

        self.dispBox = QtGui.QGroupBox(QtCore.QString("Dispersity"), self.optBox)
        self.dispBox.setMaximumWidth(150)
        self.strucLayout.addWidget(self.dispBox)
        self.dispLayout = QtGui.QHBoxLayout(self.dispBox)
        self.disperCombo = QtGui.QComboBox(self.dispBox)
        self.disperCombo.insertItem(0, QtCore.QString("Monodisperse"))
        self.disperCombo.insertItem(1, QtCore.QString("Gaussian poly."))
        self.disperCombo.insertItem(2, QtCore.QString("Exponential poly."))
        self.disperCombo.insertItem(3, QtCore.QString("Schulz-Flory poly."))
        # self.compDisable.append(self.disperCombo)
        self.connect(self.disperCombo, QtCore.SIGNAL("activated(int)"), partial(self.sel_disper, self.disperCombo))
        self.disperCombo.setCurrentIndex(2)
        self.dispLayout.addWidget(self.disperCombo)

        self.sfDefBox = QtGui.QGroupBox(QtCore.QString("SF definition"), self.optBox)
        self.sfDefBox.setMaximumWidth(150)
        self.strucLayout.addWidget(self.sfDefBox)
        self.sfDefLayout = QtGui.QHBoxLayout(self.sfDefBox)
        self.sfCombo = QtGui.QComboBox(self.sfDefBox)
        self.sfCombo.insertItem(0, QtCore.QString("MCT theory"))
        self.sfCombo.insertItem(1, QtCore.QString("ParaCr. theory"))
        self.connect(self.sfCombo, QtCore.SIGNAL("activated(int)"), partial(self.sel_sfdef, self.sfCombo))
        self.sfDefLayout.addWidget(self.sfCombo)

        self.regmBox = QtGui.QGroupBox(QtCore.QString("Regime"), self.optBox)
        self.regmBox.setMaximumWidth(150)
        self.strucLayout.addWidget(self.regmBox)
        self.regmLayout = QtGui.QHBoxLayout(self.regmBox)
        self.regmCombo = QtGui.QComboBox(self.regmBox)
        self.regmCombo.insertItem(0, QtCore.QString("MicroDomains"))
        if len(parsCurr[0]) == 2:
            self.regmCombo.setDisabled(True)
        if len(parsCurr[0]) > 2:
            self.regmCombo.insertItem(1, QtCore.QString("NanoDomains"))
        self.connect(self.regmCombo, QtCore.SIGNAL("activated(int)"), partial(self.sel_regime, self.regmCombo))
        self.regmLayout.addWidget(self.regmCombo)

        self.algoBox = QtGui.QGroupBox(QtCore.QString("Fit. algorithm"), self.optBox)
        self.algoBox.setMaximumWidth(200)
        self.fitLayout.addWidget(self.algoBox)
        self.algoLayout = QtGui.QHBoxLayout(self.algoBox)
        self.difChkBox = QtGui.QCheckBox(QtCore.QString("Diff. evolution"), self.algoBox)
        self.connect(self.difChkBox, QtCore.SIGNAL("stateChanged(int)"), partial(self.diff_evo_ch, self.difChkBox))
        self.compDisable.append(self.difChkBox)
        self.algoLayout.addWidget(self.difChkBox)
        self.minuitChkBox = QtGui.QCheckBox(QtCore.QString("MINUIT"), self.algoBox)
        self.connect(self.minuitChkBox, QtCore.SIGNAL("stateChanged(int)"), partial(self.minuit_ch, self.minuitChkBox))
        self.compDisable.append(self.minuitChkBox)
        self.algoLayout.addWidget(self.minuitChkBox)

        self.rangeBox = QtGui.QGroupBox(QtCore.QString("q-values range"), self.optBox)
        self.rangeBox.setMaximumWidth(200)
        self.fitLayout.addWidget(self.rangeBox)
        self.rangeLayout = QtGui.QHBoxLayout(self.rangeBox)
        self.lblMin = QtGui.QLabel("qmin:", self.rangeBox)
        self.rangeLayout.addWidget(self.lblMin)
        self.qMinLine = QtGui.QLineEdit(self.rangeBox)
        self.qMinLine.setText("{0:.3g}".format(qlims[0]))
        self.connect(self.qMinLine, QtCore.SIGNAL("editingFinished()"), partial(self.update_lims, self.qMinLine, 0))
        self.qMinLine.setMaximumWidth(50)
        self.rangeLayout.addWidget(self.qMinLine)
        self.lblMax = QtGui.QLabel("qmax:", self.rangeBox)
        self.rangeLayout.addWidget(self.lblMax)
        self.qMaxLine = QtGui.QLineEdit(self.rangeBox)
        self.qMaxLine.setText("{0:.3g}".format(qlims[1]))
        self.connect(self.qMaxLine, QtCore.SIGNAL("editingFinished()"), partial(self.update_lims, self.qMaxLine, 1))
        self.rangeLayout.addWidget(self.qMaxLine)

        self.fitBox = QtGui.QGroupBox(QtCore.QString("Fit controls"), self.centralwidget)
        self.controlsLayout = QtGui.QGridLayout(self.fitBox)
        self.centralLayout.addWidget(self.fitBox)
        self.buttDraw = QtGui.QPushButton(QtCore.QString("Re-Draw All"), self.fitBox)
        self.connect(self.buttDraw, QtCore.SIGNAL("clicked()"), self.on_draw)
        self.controlsLayout.addWidget(self.buttDraw, 0, 0)
        self.buttFit = QtGui.QPushButton(QtCore.QString("Fit"), self.fitBox)
        self.connect(self.buttFit, QtCore.SIGNAL("clicked()"), self.on_fit)
        self.controlsLayout.addWidget(self.buttFit, 0, 1)
        self.buttTerm = QtGui.QPushButton(QtCore.QString("STOP"), self.fitBox)
        self.connect(self.buttTerm, QtCore.SIGNAL("clicked()"), self.on_terminate)
        self.controlsLayout.addWidget(self.buttTerm, 0, 2)
        self.buttBackStep = QtGui.QPushButton(QtCore.QString("Step back"), self.fitBox)
        self.connect(self.buttBackStep, QtCore.SIGNAL("clicked()"), self.step_back)
        self.controlsLayout.addWidget(self.buttBackStep, 0, 3)
        self.buttReset = QtGui.QPushButton(QtCore.QString("Reset to:"), self.fitBox)
        self.compDisable.append(self.buttReset)
        self.connect(self.buttReset, QtCore.SIGNAL("clicked()"), self.on_reset)
        self.controlsLayout.addWidget(self.buttReset, 0, 4)
        self.resetLine = QtGui.QLineEdit(QtCore.QString("0"), self.fitBox)
        self.resetLine.setMaximumWidth(30)
        self.resetLine.setValidator(QtGui.QIntValidator(0, 1000))
        self.controlsLayout.addWidget(self.resetLine, 0, 5)
        self.label = QtGui.QLabel(QtCore.QString(". step"), self.fitBox)
        self.controlsLayout.addWidget(self.label, 0, 6)
        self.controlsLayout.setColumnStretch(10, 2)

    def save_progress(self):
        """
        save the current state plus history into a file, which can be loaded later and allow to continue on fitting
        :return: NULL
        """
        global parsHist
        global fitParLims

        try:
            savename = QtGui.QFileDialog.getSaveFileName()
            savefile = open(savename, 'wb')
            self.textBox.append(strftime("\n\nSaved on %a, %d %b %Y at %H:%M:%S\n\n"))
            textwin = self.textBox.toPlainText()
            cp.dump(textwin, savefile, protocol=-1)
            cp.dump(parsHist, savefile, protocol=-1)
            cp.dump(fitParLims, savefile, protocol=-1)
            savefile.close()
        except IOError:
            print "Check the input!"
            return 1

    def load_progress(self):
        """
        load saved progress from a chosen file. file has to have the same structure as created in saveProgress() routine
        :return: NULL
        """
        global parsHist, parsCurr, fitParLims
        global fitWins

        try:
            loadname = QtGui.QFileDialog.getOpenFileName()
            loadfile = open(loadname, 'rb')
            textwin = cp.load(loadfile)
            mpars = cp.load(loadfile)
            mplims = cp.load(loadfile)
            parsHist = copy.deepcopy(mpars)
            parsCurr = copy.deepcopy(mpars[-1])
            pp.pprint(parsCurr)
            fitParLims = np.nan_to_num(mplims)
            fitParLims.fill(None)
            self.difChkBox.setChecked(False)
            self.minuitChkBox.setChecked(False)
            self.updBox()
            self.textBox.clear()
            self.textBox.setPlainText(textwin)
            self.textBox.append("\n----- Data successfully loaded -----\n\n")
        except IOError:
            pass

    def export_pars(self):
        """
        manu action exporting parameter values into a new parameter file.
        :return: NULL
        """
        global parsCurr

        loc_pars = copy.deepcopy(parsCurr)
        try:
            self.saveDir = QtGui.QFileDialog.getExistingDirectory()
            for i in range(len(loc_pars)):
                loc_pars[i]['0']['Prps']['Bckg'] = np.ndarray.tolist(loc_pars[i]['0']['Prps']['Bckg'])
                self.fileName = self.saveDir + "\pars_exp_nr" + str(i+1) + ".json"
                self.saveFile = open(self.fileName, 'w')
                js.dump(loc_pars[i], self.saveFile, indent=4, sort_keys=True)
                self.textBox.append("Parameters successfully exported.\n")
                self.saveFile.close()
        except:
            pass

    def updBox(self):
        """
        update all values in all fitWins
        :return: NULL
        """
        global fitWins
        global parsHist

        self.resetLine.setText(str(len(parsHist) - 1))

        for win in fitWins:
            win.updBox()
            win.plot_update()
        self.textBox.append("\nValues updated.")

    def sel_disper(self, combo):
        """
        set the system mono/polydispersity and its distribution
        :param combo: number of a choice
        :return: NULL
        """
        global polydispersity, fitDist
        global fitWins

        if combo.currentIndex() == 0:
            polydispersity = 0
            fitDist = 0
            for fitwin in fitWins:
                for chk in fitwin.polyDispSigChks:
                    chk.setChecked(False)
                    chk.setDisabled(True)
        if combo.currentIndex() == 1:
            polydispersity = 1
            fitDist = 0
            for fitwin in fitWins:
                for chk in fitwin.polyDispSigChks:
                    chk.setChecked(False)
                    chk.setDisabled(True)
        if combo.currentIndex() == 2:
            polydispersity = 1
            fitDist = 1
            for fitwin in fitWins:
                for chk in fitwin.polyDispSigChks:
                    chk.setChecked(False)
                    chk.setDisabled(True)
        if combo.currentIndex() == 3:
            polydispersity = 1
            fitDist = 2
            for fitwin in fitWins:
                for chk in fitwin.polyDispSigChks:
                    chk.setEnabled(True)

        for win in fitWins:
            win.plot_update()

    def sel_sfdef(self, combo):
        """
        a switch to select a structure factor the system (as the whole): MCT or paracrystalline theory
        :param combo: number of choice
        :return: NULL
        """
        global fitSFDef
        global fitWins

        fitSFDef = int(combo.currentIndex())

        for win in fitWins:
            win.plot_update()

    def sel_regime(self, combo):
        """
        a switch to select a type of regime for domains: micro/nano
        :param combo: number of a choice
        :return: NULL
        """
        global domainRegime
        global fitWins

        domainRegime = int(combo.currentIndex())

        for win in fitWins:
            for item in win.regimeDisables:
                if domainRegime == 0:
                    item.setEnabled(True)
                if domainRegime == 1:
                    item.setDisabled(True)

        for win in fitWins:
            win.plot_update()

    def update_lims(self, line, index):
        """
        function updates global variable qlims and replots all plots displaying the range
        :return: NULL
        """
        global qlims
        global fitWins

        qlims[index] = float(eval(str(line.text())))
        for win in fitWins:
            win.plot_update()

    def on_draw(self):
        """
        function to redraw all plots in the fitWins
        :return: NULL
        """
        global fitWins

        for win in fitWins:
            win.plot_update()

    def on_fit(self):
        """
        all orders run on fit
        :return: NULL
        """
        global qlims, bupqlims
        global parsCurr, bupParsCurr
        global fitParLims, bupFitParLims
        global fitParSwitch, bupFitPSwitch
        global trmnt

        trmnt = False

        self.resFit = []

        self.fitParNames = []
        self.fitParVals = []
        self.fitParErrs = []
        self.fitParLims = []

        bupqlims = copy.deepcopy(qlims)
        bupParsCurr = copy.deepcopy(parsCurr)
        bupFitParLims = copy.deepcopy(fitParLims)
        bupFitPSwitch = copy.deepcopy(fitParSwitch)

        parsfit = bupFitPSwitch.nonzero()[0]

        for idx in parsfit:
            self.fitParVals.append(getFromDict(bupParsCurr, fitParAddrs[idx] + [0]))
            if len(fitParAddrs[idx]) == 4:
                self.fitParNames.append("Ser" + str(fitParAddrs[idx][0]) + "_" + fitParAddrs[idx][2]
                                        + "_" + fitParAddrs[idx][3] + "_" + fitParAddrs[idx][1] +
                                        "phs")
            if len(fitParAddrs[idx]) == 5:
                self.fitParNames.append("Ser" + str(fitParAddrs[idx][0]) + "_" + fitParAddrs[idx][2]
                                        + "_" + fitParAddrs[idx][3] + "_" + str(fitParAddrs[idx][4]) + "_"
                                        + fitParAddrs[idx][1] + "phs")
            self.fitParErrs.append(getFromDict(bupParsCurr, fitParAddrs[idx] + [1]))
            if not np.isnan(bupFitParLims[idx][0]):
                self.fitParLims.append(bupFitParLims[idx])
            else:
                self.fitParLims.append(None)

        self.resFit.append(self.fitParNames)
        self.resFit.append(self.fitParVals)
        self.resFit.append(self.fitParErrs)
        self.resFit.append(self.fitParLims)

        self.textBox.append("-------------------")
        self.textBox.append(" Step nr. %d" % (len(parsHist)))

        self.textBox.append(QtCore.QString("<font color='blue'>Initialization</font>"))
        self.fitThrd = fitThread(devfit=self.devAlg, minfit=self.minAlg)
        self.connect(self.fitThrd, QtCore.SIGNAL("fit_ended()"), self.after_fit)
        self.connect(self.fitThrd, QtCore.SIGNAL("no_free_pars()"), partial(self.on_terminate,
                    "<font color='red'>\nNo free parameters to fit!\n</font>"))
        self.connect(self.fitThrd, QtCore.SIGNAL("no_pars()"), partial(self.on_terminate,
                    "<font color='red'>\nNo variables to fit!\n</font>"))
        self.connect(self.fitThrd, QtCore.SIGNAL("no_meth()"), partial(self.on_terminate,
                    "<font color='red'>\nNo fitting method chosen!\n</font>"))

        if len(self.fitParVals) == 0:
            trmnt = True
            self.textBox.append("<font color='red'>No pars to fit.</font>")

        frprs = False
        for i in range(len(self.fitParLims)):
            if self.fitParLims[i] is not None:
                nfrprs = self.fitParLims[i][1] == self.fitParLims[i][0]
            else:
                nfrprs = False
            frprs = frprs or (not nfrprs)
        if not frprs:
            trmnt = True
            self.textBox.append("<font color='red'>No pars to fit.</font>")

        if not trmnt:
            self.fitThrd.start()
            self.textBox.append(QtCore.QString("<font color='blue'>Fitting...</font>"))

    def on_terminate(self, msg=None):
        """
        instructions for the termination of the fitting process
        :return: NULL
        """
        global trmnt
        global fitalg

        fitalg = 0

        try:
            trmnt = True
            if msg is not None:
                self.textBox.append(msg)
            self.fitThrd.exit(1)
            self.fitThrd.wait()
            del self.fitThrd
            self.fitThrd = None
            print "Stopping fit...\n"
            self.textBox.append(QtCore.QString("<font color='red'>Fitting terminated!</font>"))
        except AttributeError:
            self.textBox.append("No running fit.")

    def after_fit(self):
        global fitWins
        global fitalg
        global parsHist
        global parsCurr
        global bupParsCurr
        global bupFitPSwitch

        print "Fit done."

        self.textBox.append("\nFit done.")

        parsCurr = fitp2modp(self.resFit[1], bupParsCurr, self.resFit[2])
        parsHist.append(copy.deepcopy(parsCurr))

        self.fitThrd = None

        self.updBox()

    def step_back(self):
        """
        function moving a step back in history
        :return:
        """
        global fitWins
        global parsHist
        global parsCurr

        try:
            parsCurr = copy.deepcopy(parsHist[-2])
            parsHist = parsHist[:-1]
            self.resetLine.setText(str(len(parsHist) - 1))
            for win in fitWins:
                win.updBox()
                win.plot_update()
            self.textBox.append("-------------------------")
            self.textBox.append("      Moved back         ")
            self.textBox.append("-------------------------")
            pass
        except IndexError:
            self.textBox.append("<font color='red'>   Something went wrong.</font>")
            self.textBox.append("<font color='red'>   Sorry. Ofc.</font>")

    def on_reset(self):
        """
        instruction for the reset of fitting history
        :return:
        """
        global parsHist
        global parsCurr
        global fitWins

        try:
            step = int(self.resetLine.text())
            parsCurr = copy.deepcopy(parsHist[step])
            parsHist = parsHist[:step+1]
            for win in fitWins:
                win.updBox()
                win.plot_update()
            self.textBox.append("---------------------------")
            self.textBox.append("   RESET to %d. step " % step)
            self.textBox.append("---------------------------")
            pass
        except IndexError:
            self.textBox.append("---------------------------")
            self.textBox.append("<font color='red'>   Error on reset:</font>")
            self.textBox.append("<font color='red'>   Wrong value of step.</font>")
            self.textBox.append("---------------------------")
            pass

        for win in fitWins:
            win.updBox()
            win.plot_update()

    def diff_evo_ch(self, chbox):
        """
        differential evolution algorithm switch
        :param chbox: watched diffevo-checkbox
        :return: NULL
        """
        global fitWins
        global fitParSwitch

        self.devAlg = bool(chbox.isChecked())

        for win in fitWins:
            win.set_lim_chks()
            for lchk in win.limChks:
                lchk.setDisabled(chbox.isChecked())

    def minuit_ch(self, chbox):
        """
        minuit fitting algorithm switch
        :param chbox: watched minuit-checkbox
        :return: NULL
        """
        self.minAlg = bool(chbox.isChecked())

    def show_all(self):
        """
        show all hidden/closed fit windows
        :return: NULL
        """
        global fitWins
        global mainwin

        mainwin.show()
        for win in fitWins:
            win.show()

    def set_densities(self):
        """
        function bringing a window through which one can easily set the densities of water and
        hydrocarbon chains environments
        :return: NULL
        """
        global densWin

        densWin = Densities()

        densWin.show()

# ---------- End of function definitions -----------------------

# TODO put a signal icon or a flag into the main window to indicate if fitting procedure is running or not

# TODO coollect all internal parameters and 'switches' at one place (perhaps here)
eulcon = 0.5772156649

# divadm: switch to divide cholesterol into a head and a tail
divadm = False

# regime of domains: 0 - microscopic, 1 - nanoscopic
domainRegime = 0

# switch to keep the relative distances of headgroup components to hydrocarbon cores constant
connheadson = False
fixheadson = False

# switch to keep the relative distances of admixtures to the hydrocarbon chains core border constant
conn_adm_on = False

# list of all files and directories in the working directory
dirlist = os.listdir('.')

# fitting parameters

trmnt = False  # fitting termination switch

polydispersity = 1  # 0 - mono, 1 - poly
fitalg = 0  # fitting algorithm: 0 - LM, 1 - GA
fitDist = 1  # 0 - gauss, 1 - exponential, 2 - schulz-flory(gamma)
fitSFDef = 0  # structure factor definition: 0 - MCT theory, 1 - PT theory

d = 63.0
dens_w = 0.333  # electron density of water

# like a history of fitting steps
parsHist = []  # history of all successful parameter iterations

parsCurr = []

# defining component groups attached to hydrocarbon cores
# list contains only final keys, not the whole 'addresses'
connkeys = []


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)

    metwin = MethWin()
    metwin.show()

    loadwin = LoadData()

    densWin = None

    mainwin = None

    fitWins = []

    sys.exit(app.exec_())
