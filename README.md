# **G**lobal **A**nalysis **P**rogram

![icon.png](./images/gap_image.png)

*Being developed and mantained at Institute of Molecular Biosciences, (Graz, Austria) in the group of Georg Pabst.*

A windows executable, can be found in the [download section][downloads]. To run the application just unpack the downloaded .7z file (using [7zip][7zip] or any unzipping application) and run 'GAP.exe' from the forlder.

GAP is written in Python2.7. If you want to run it from the [source code][sourcepage], following additional python libraries are required to be installed.

**Python modules:**

* guidata 1.6.1
* guiqwt 2.3.2
* iminuit 1.1.1
* numpy 1.11.2
* PyQt4 4.11.3
* scipy 0.16.1

Details regarding its run and (pre-)configuration can be found in the [wiki][wikihome].

Good luck!

[downloads]: https://bitbucket.org/michalbel/gap/downloads/
[wikihome]: https://bitbucket.org/michalbel/gap/wiki/Home/
[issuepage]: https://bitbucket.org/michalbel/gap/issues/
[7zip]: http://www.7-zip.org/
[sourcepage]: https://bitbucket.org/michalbel/gap/src/